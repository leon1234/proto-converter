# protosdc-converter

To reduce the barrier of entry and avoid an inconsistent representation of the BICEPS data model in protoSDC, the entire
XML Schema for BICEPS is automatically converted into protobuf as well as additional protoSDC target languages. This
ensures a high degree of compatibility between protoSDC implementations as well as a very low barrier of entry.

For more information visit https://converter.protosdc.org