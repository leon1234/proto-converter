package org.somda.protosdc.mapping.base

import com.google.protobuf.*
import com.google.protobuf.Any
import org.apache.logging.log4j.kotlin.Logging
import java.lang.Exception
import java.math.BigDecimal
import java.math.BigInteger
import java.net.URI
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.time.YearMonth
import kotlin.time.Duration
import javax.xml.namespace.QName
import kotlin.time.Duration.Companion.nanoseconds

typealias ProtoToKotlinAnyHandler = (value: Any) -> kotlin.Any

@Suppress("MemberVisibilityCanBePrivate", "unused")
object ProtoToKotlinBaseTypes : Logging {
    private var anyHandler: ProtoToKotlinAnyHandler? = null

    fun mapURI(uri: String): URI {
        return try {
            URI(uri)
        } catch (e: Exception) {
            logger.error { "Error mapping to URI: $e" }
            throw e
        }
    }

    fun mapUInt64Value(value: UInt64Value): Long {
        return value.value
    }

    fun mapUInt32Value(value: UInt32Value): Int {
        return value.value
    }

    fun mapStringValue(value: StringValue): String {
        return value.value
    }

    fun mapInt64Value(value: Int64Value): Long {
        return value.value
    }

    fun mapInt32Value(value: Int32Value): Int {
        return value.value
    }

    fun mapBoolValue(value: BoolValue): Boolean {
        return value.value
    }

    fun mapDuration(value: com.google.protobuf.Duration): Duration {
        return (value.seconds * 1_000_000_000).nanoseconds + value.nanos.nanoseconds
    }

    fun mapBigInteger(value: String): BigInteger {
        return BigInteger(value)
    }

    fun mapOptionalQName(value: StringValue): QName {
        return mapQName(mapStringValue(value))
    }

    fun mapBigDecimal(value: String): BigDecimal {
        return BigDecimal(value)
    }

    fun mapOptionalBigDecimal(value: StringValue): BigDecimal {
        return BigDecimal(mapStringValue(value))
    }

    fun mapOptionalUri(value: StringValue): URI {
        return mapURI(mapStringValue(value))
    }

    fun mapLocalDateTime(value: String): LocalDateTime {
        return LocalDateTime.parse(value)
    }

    fun mapLocalDate(value: String): LocalDate {
        return LocalDate.parse(value)
    }

    fun mapYearMonth(value: String): YearMonth {
        return YearMonth.parse(value)
    }

    fun mapYear(value: String): Year {
        return Year.parse(value)
    }

    fun mapQName(value: String): QName {
        return QName.valueOf(value)
    }

    fun mapOptionalLocalDateTime(value: StringValue): LocalDateTime {
        return mapLocalDateTime(value.value)
    }

    fun mapAny(value: Any): kotlin.Any {
        val localAnyHandler = anyHandler
        return localAnyHandler?.let { localAnyHandler(value) } ?: value
    }

    fun registerAnyHandler(anyHandler: ProtoToKotlinAnyHandler) {
        this.anyHandler = anyHandler
    }

}