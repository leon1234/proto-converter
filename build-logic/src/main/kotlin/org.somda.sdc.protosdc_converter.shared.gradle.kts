import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    idea
    java
    `java-library`

    kotlin("jvm")
    kotlin("plugin.serialization")
}

group = "org.somda.sdc.protosdc_converter"
val baseVersion = "0.0.14"
val isSnapshot: Boolean = hasProperty("snapshot")
val buildId: String? = System.getenv("CI_PIPELINE_IID")

version = when (isSnapshot) {
    true -> "$baseVersion-SNAPSHOT" + (buildId?.let { "+$buildId" } ?: "")
    false -> baseVersion
}

if (baseVersion != version) {
    logger.warn(
        "Changed version from '$baseVersion' to '$version'"
    )
} else {
    logger.warn(
        "Keep version '$baseVersion'"
    )
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation(libs.bundles.logging)
    implementation(libs.serialization.json)

    testImplementation(libs.bundles.testing)
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

val jdkVersion: JvmTarget = JvmTarget.JVM_17

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(jdkVersion.target)
    }
    withSourcesJar()
}

kotlin {
    // todo enable explicit api
    //explicitApi()
    compilerOptions {
        jvmTarget = jdkVersion
    }
}