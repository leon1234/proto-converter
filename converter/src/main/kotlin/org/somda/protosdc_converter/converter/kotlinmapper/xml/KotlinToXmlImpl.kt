package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.kotlin.KotlinEntry
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlin.kotlinLanguageType
import org.somda.protosdc_converter.converter.kotlin.typeName
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.converter.kotlinmapper.proto.KotlinProtoMappingSource
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.XmlType
import java.io.PrintWriter
import javax.xml.namespace.QName

/**
 * Generates mapping functions for the mapping direction Kotlin -> XML.
 *
 * TODO: this class is a mess and needs to be cleaned up by removing redundant code segments
 */
internal class KotlinToXmlImpl(
    private val kotlinSource: KotlinSource,
    private val kotlinMappingSource: KotlinXmlMappingSource,
    private val util: KotlinXmlMapperUtil
) {
    private val kotlinToXmlMapperMap = util.kotlinToXmlMapperMap

    /**
     * Prints the mapping functions related to the given node to the given target print writer.
     *
     * @param node the root base node or currently processed node.
     * @param target the writer to which code will be generated.
     * @param level indentation offset for output.
     * @param parentNode parent node if present.
     */
    fun processNode(node: BaseNode, target: PrintWriter, level: Int = 0, parentNode: BaseNode? = null) {
        if (node.ignore) {
            return
        }

        node.clusteredTypes?.let { cluster ->
            if (cluster.isEmpty()) {
                return@let
            }
            logger.debug { "Encountered cluster ${node.clusteredTypes}" }

            if (node.kotlinToXmlClusterHandled()) {
                logger.debug { "Encountered cluster ${node.clusteredTypes} is already handled" }
                return@let
            }

            logger.debug { "Encountered cluster ${node.clusteredTypes} is new" }
            logger.debug { "Add cluster mappers all at once to map" }

            (listOf(node) + cluster).forEach { clusterType ->
                clusterType.kotlinToXmlSetClusterHandled()

                // create mappers
                val xmlName = util.fullyQualifiedXmlNameFor(clusterType)
                val kotlinName = util.fullyQualifiedKotlinNameFor(clusterType)
                val functionName =
                    kotlinMappingSource.mapFunctionName(KotlinProtoMappingSource.functionNameOfPackage(kotlinName))
                kotlinToXmlMapperMap[kotlinName, xmlName] = functionName
            }
        }

        val kotlinLanguageType = node.kotlinLanguageType()
        val qualifiedXmlTypeName = util.fullyQualifiedXmlNameFor(node)
        val qualifiedKotlinTypeName = util.fullyQualifiedKotlinNameFor(node)
        val mapper = kotlinToXmlMapperMap[qualifiedKotlinTypeName, qualifiedXmlTypeName]

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {

                if (mapper != null && node.clusteredTypes == null) {
                    logger.debug {
                        "Skip generating mapper from non-clustered message $qualifiedXmlTypeName to " +
                                "$qualifiedKotlinTypeName, already present"
                    }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toSet()
                val nestedTypes = node.children - parameters

                val kotlinLanguageTypeSkip = when (kotlinLanguageType) {
                    is KotlinEntry.DataClass -> kotlinLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in kotlin
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && kotlinLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNode(child, target, level, node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName = kotlinMappingSource.mapFunctionName(
                    KotlinXmlMappingSource.functionNameOfPackage(
                        qualifiedKotlinTypeName
                    )
                )

                kotlinToXmlMapperMap[qualifiedKotlinTypeName, qualifiedXmlTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.kotlinToXmlFunctionStart(
                        functionName = functionName,
                        targetType = KotlinType.DOM_NODE,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                // only for non oneOfs
                if (!isOneOf && !node.isCustomType) {
                    parameters.forEach { child ->
                        processNode(child, target, level + 1, node)
                    }
                }

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.returnTarget()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            is NodeType.Parameter -> {
                check(qualifiedXmlTypeName == qualifiedKotlinTypeName || mapper != null) {
                    "Cannot map from parameter $qualifiedKotlinTypeName to $qualifiedXmlTypeName, no mapper!"
                }

                when {
                    // there is no such thing as an optional (nullable) list, so these can be merged
                    nodeType.list -> when (node.originalXmlNode?.xmlType) {
                        is XmlType.List -> {
                            // add as new node to target
                            target.println(kotlinSource.indentLines(level) {
                                kotlinMappingSource.kotlinListToXmlList(
                                    kotlinField = util.getKotlinFieldName(kotlinLanguageType),
                                    mappingFunction = mapper
                                )
                            })
                        }

                        else -> {
                            target.println(kotlinSource.indentLines(level) {
                                kotlinMappingSource.kotlinListToXmlSequence(
                                    xmlQName = node.originalXmlNode!!.qName!!,
                                    kotlinField = util.getKotlinFieldName(kotlinLanguageType),
                                    mappingFunction = mapper
                                )
                            })
                        }
                    }

                    nodeType.optional -> {
                        when (nodeType.wasAttribute) {
                            true -> {
                                target.println(kotlinSource.indentLines(level) {
                                    node.originalXmlNode!!.qName!!.let { qName ->
                                        kotlinMappingSource.optionalKotlinToXmlAttribute(
                                            util.getKotlinFieldName(kotlinLanguageType),
                                            qName,
                                            mapper
                                        )
                                    }
                                })
                            }

                            else -> {
                                target.println(kotlinSource.indentLines(level) {
                                    node.originalXmlNode!!.qName!!.let { qName ->
                                        kotlinMappingSource.optionalKotlinFieldToXmlElement(
                                            util.getKotlinFieldName(kotlinLanguageType),
                                            qName,
                                            mapper
                                        )
                                    }
                                })
                            }
                        }
                    }

                    else -> {
                        val kotlinParam = kotlinLanguageType as KotlinEntry.KotlinParameter
                        when (nodeType.wasAttribute) {
                            true -> {
                                when (node.originalXmlNode!!.xmlType) {
                                    is XmlType.AttributeGroupRef -> {
                                        target.println(kotlinSource.indent(level) {
                                            kotlinMappingSource.kotlinToXmlAttributeGroup(
                                                mapper,
                                                kotlinParam.parameterName
                                            )
                                        })
                                    }

                                    else -> {
                                        target.println(kotlinSource.indentLines(level) {
                                            node.originalXmlNode!!.qName!!.let { qName ->
                                                kotlinMappingSource.kotlinToXmlAttribute(
                                                    util.getKotlinFieldName(kotlinLanguageType), qName, mapper
                                                )
                                            }
                                        })
                                    }
                                }
                            }

                            else -> {
                                when (val qName = node.originalXmlNode?.qName) {
                                    null -> when (node.originalXmlNode!!.xmlType) {
                                        is XmlType.SimpleContentExtension, is XmlType.SimpleType ->
                                            target.println(kotlinSource.indentLines(level) {
                                                kotlinMappingSource.requiredKotlinFieldToXmlText(
                                                    kotlinParam.parameterName,
                                                    mapper
                                                )
                                            })

                                        else ->
                                            target.println(kotlinSource.indent(level) {
                                                kotlinMappingSource.kotlinToXml(mapper, kotlinParam.parameterName)
                                            })
                                    }

                                    else -> {
                                        val parentType = parentNode?.originalXmlNode?.xmlType
                                        when (parentType is XmlType.Element && parentType.type != null) {
                                            true -> {
                                                // is element with type ref, just call mapper function of type
                                                target.println(kotlinSource.indent(level) {
                                                    kotlinMappingSource.kotlinToXml(
                                                        mapper,
                                                        util.getKotlinFieldName(kotlinLanguageType)
                                                    )
                                                })
                                            }

                                            false -> {
                                                // is content/type/element w/o type ref; find element to map
                                                target.println(kotlinSource.indentLines(level) {
                                                    kotlinMappingSource.requiredKotlinFieldToXmlElement(
                                                        util.getKotlinFieldName(kotlinLanguageType), qName, mapper
                                                    )
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            is NodeType.StringEnumeration -> {
                // create mapper
                val functionName = kotlinMappingSource.mapFunctionName(
                    KotlinProtoMappingSource.functionNameOfPackage(
                        qualifiedKotlinTypeName
                    )
                )

                kotlinToXmlMapperMap[qualifiedKotlinTypeName, qualifiedXmlTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.kotlinToXmlFunctionStart(
                        functionName = functionName,
                        targetType = KotlinType.DOM_NODE,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.kotlinToXmlEnumStart()
                })

                nodeType.values.forEach { value ->
                    target.println(kotlinSource.indent(level + 2) {
                        kotlinMappingSource.kotlinToXmlEnumCase(
                            sourceType = qualifiedKotlinTypeName,
                            sourceValue = value,
                            targetValue = value
                        )
                    })
                }

                target.println(kotlinSource.indent(level + 2) {
                    kotlinMappingSource.xmlToKotlinEnumElse()
                })
                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.xmlToKotlinEnumEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            is NodeType.BuiltinType -> {
                logger.debug { "Skipping builtin type ${node.nodeName}" }
            }

            is NodeType.OneOf -> {
                // retrieve the fully qualified parent node name here for proto
                val oneOfXmlTypeName = when (node.originalXmlNode?.xmlType) {
                    is XmlType.Union -> util.fullyQualifiedXmlNameFor(parentNode!!)
                    else -> if (nodeType.defaultTypeParameter == null) {
                        util.fullyQualifiedXmlNameFor(parentNode!!)
                    } else {
                        util.fullyQualifiedXmlNameFor(nodeType.defaultTypeParameter!!) + "OneOf"
                    }
                }

                // create mapper
                val functionName = kotlinMappingSource.mapFunctionName(
                    KotlinXmlMappingSource.functionNameOfPackage(
                        qualifiedKotlinTypeName
                    )
                )

                kotlinToXmlMapperMap[qualifiedKotlinTypeName, oneOfXmlTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.kotlinToXmlFunctionStart(
                        functionName = functionName,
                        targetType = KotlinType.DOM_NODE,
                        sourceType = qualifiedKotlinTypeName,
                    )
                })

                when (node.originalXmlNode?.xmlType) {
                    is XmlType.Union -> {
                        target.println(kotlinSource.indent(level + 1) {
                            kotlinMappingSource.kotlinToXmlOneOfStart()
                        })
                        node.children.forEach { child ->
                            check(child.nodeType is NodeType.Parameter)

                            val kotlinChild = child.kotlinLanguageType()

                            check(kotlinChild is KotlinEntry.KotlinOneOfParameter)

                            val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                            val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                            // generate oneof parameters
                            val childMapper = kotlinToXmlMapperMap[childKotlinTypeName, childXmlTypeName]

                            check(childXmlTypeName == childKotlinTypeName || childMapper != null) {
                                "Cannot map from oneOf parameter $childKotlinTypeName to $childXmlTypeName, no mapper!"
                            }

                            val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                qualifiedKotlinTypeName,
                                kotlinMappingSource.oneOfKotlinEntryName(kotlinChild.kotlinType.typeName())
                            )

                            target.println(kotlinSource.indent(level + 2) {
                                kotlinMappingSource.kotlinToXmlUnionCase(
                                    kotlinChoice = "is $kotlinCaseName",
                                    mappingFunction = childMapper
                                )
                            })
                        }
                        target.println(kotlinSource.indent(level + 1) {
                            kotlinSource.blockEnd()
                        })
                    }

                    else -> {
                        when (val defaultNode = nodeType.defaultTypeParameter) {
                            null -> {
                                val choiceNode = nodeType.parentNode!!
                                val choiceKotlinEntry = choiceNode.kotlinLanguageType()
                                check(choiceKotlinEntry is KotlinEntry.DataClass)

                                val choiceXmlTypeName = util.fullyQualifiedXmlNameFor(choiceNode)
                                val choiceKotlinTypeName = util.fullyQualifiedKotlinNameFor(choiceNode)
                                val choiceMapper = kotlinToXmlMapperMap[choiceKotlinTypeName, choiceXmlTypeName]

                                checkNotNull(choiceMapper != null) {
                                    "Cannot map choice from $choiceKotlinTypeName to $choiceXmlTypeName, no mapper!"
                                }

                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinMappingSource.kotlinToXmlOneOfStart()
                                })
                                node.children.forEach { child ->
                                    check(child.nodeType is NodeType.Parameter)

                                    val kotlinChild = child.kotlinLanguageType()

                                    check(kotlinChild is KotlinEntry.KotlinOneOfParameter)

                                    val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                                    val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                                    // generate oneof parameters
                                    val childMapper = kotlinToXmlMapperMap[childKotlinTypeName, childXmlTypeName]

                                    check(childXmlTypeName == childKotlinTypeName || childMapper != null) {
                                        "Cannot map from oneOf parameter $childKotlinTypeName to $childXmlTypeName, no mapper!"
                                    }

                                    val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                        qualifiedKotlinTypeName,
                                        kotlinMappingSource.oneOfKotlinEntryName(kotlinChild.kotlinType.typeName())
                                    )

                                    target.println(kotlinSource.indent(level + 2) {
                                        kotlinMappingSource.kotlinToXmlUnionCase(
                                            kotlinChoice = "is $kotlinCaseName",
                                            mappingFunction = childMapper
                                        )
                                    })
                                }
                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinSource.blockEnd()
                                })
                            }
                            else -> {
                                val defaultKotlinEntry = defaultNode.kotlinLanguageType()
                                check(defaultKotlinEntry is KotlinEntry.KotlinOneOfParameter)

                                val defaultXmlTypeName = util.fullyQualifiedXmlNameFor(defaultNode)

                                val defaultKotlinTypeName = util.fullyQualifiedKotlinNameFor(defaultNode)
                                val defaultMapper = kotlinToXmlMapperMap[defaultKotlinTypeName, defaultXmlTypeName]

                                check(defaultMapper != null) {
                                    "Cannot map from default oneOf parameter $defaultKotlinTypeName to $defaultXmlTypeName, no mapper!"
                                }

                                val defaultKotlinCaseName = KotlinSource.qualifiedPathFrom(
                                    qualifiedKotlinTypeName,
                                    kotlinMappingSource.oneOfKotlinEntryName(defaultKotlinEntry.kotlinType.typeName())
                                )

                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinMappingSource.kotlinToXmlInheritanceStart()
                                })

                                node.children.forEach { child ->
                                    check(child.nodeType is NodeType.Parameter)

                                    val childKotlinEntry = child.kotlinLanguageType()

                                    check(childKotlinEntry is KotlinEntry.KotlinOneOfParameter)

                                    val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                                    val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                                    // generate oneof parameters
                                    val childMapper = kotlinToXmlMapperMap[childKotlinTypeName, childXmlTypeName]

                                    check(childMapper != null) {
                                        "Cannot map from oneOf parameter $childKotlinTypeName to $childXmlTypeName, no mapper!"
                                    }

                                    val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                        qualifiedKotlinTypeName,
                                        kotlinMappingSource.oneOfKotlinEntryName(childKotlinEntry.kotlinType.typeName())
                                    )

                                    target.println(kotlinSource.indentLines(level + 2) {
                                        kotlinMappingSource.kotlinToXmlInheritanceEntry(
                                            xmlQName = QName.valueOf(childXmlTypeName),
                                            kotlinChoice = kotlinCaseName,
                                            defaultKotlinChoice = defaultKotlinCaseName,
                                            mappingFunction = childMapper
                                        )
                                    })
                                }
                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinSource.blockEnd()
                                })
                            }
                        }
                    }
                }

                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            else -> throw Exception("Cannot handle nodeType ${node.nodeType}")
        }
    }

    private companion object : Logging
}