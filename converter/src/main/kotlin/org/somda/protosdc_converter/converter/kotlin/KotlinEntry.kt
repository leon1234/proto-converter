package org.somda.protosdc_converter.converter.kotlin

import org.somda.protosdc_converter.xmlprocessor.BaseLanguageType
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import java.util.*

/**
 * Class for modelling Kotlin data classes and converting them into actual code.
 */
sealed class KotlinEntry : BaseLanguageType {
    val entryId = UUID.randomUUID().toString()

    /**
     * A type as in a data class.
     *
     * @param typeName the type name, e.g. `AbstractDescriptor`.
     * @param packagePath package path separated by dots, e.g. `org.somda.protosdc.model.biceps`.
     * @param wasAttribute true if this type was specified as an attribute in the originating XML Schema.
     * @param fileName optional file name for this type including file extension.
     * @param typeNameRaw todo why does this field exists?
     * @param nestedTypes list of types which are nested below this type and will be generated into the
     *                    body of the data class.
     * @param onlyGenerateNested marks a class to be only generated when generating nested content, i.e. when
     *                           generating the body of a data class, in which nested classes are placed.
     * @param skipType marks a type to be skipped, only generating its children. Used to un-nest wrapper types from
     *                 the abstract model.
     */
    data class DataClass(
        val typeName: String,
        var packagePath: String? = null,
        val wasAttribute: Boolean = false,
        var fileName: String? = null,
        val typeNameRaw: String = typeName,
        val nestedTypes: MutableList<BaseNode> = mutableListOf(),
        var onlyGenerateNested: Boolean = false,
        var skipType: Boolean = false
    ) : KotlinEntry() {
        fun importPath(): String = packagePath?.let { "$it.$typeName" } ?: typeName
    }

    /**
     * A parameter within a OneOf.
     *
     * A OneOf parameter has different generating rules from [KotlinParameter].
     *
     * @param kotlinType the type of the parameter.
     * @param parameterName name of the parameter.
     * @param oneOf the OneOf to which this parameter belongs to.
     */
    data class KotlinOneOfParameter(
        val kotlinType: KotlinEntry,
        val parameterName: String,
        val oneOf: KotlinOneOf
    ) : KotlinEntry()

    /**
     * A parameter in a data class.
     *
     * @param kotlinType the type of the parameter.
     * @param parameterName name of the parameter.
     * @param list true if this parameter is a list, false otherwise
     * @param nullable true if this parameter is nullable, false otherwise.
     *                 Note that lists are typically not nullable and this parameter may be ignored for lists.
     */
    data class KotlinParameter(
        val kotlinType: KotlinEntry,
        val parameterName: String,
        val list: Boolean = false,
        val nullable: Boolean = false,
    ) : KotlinEntry()

    /**
     * A string enumeration.
     *
     * @param enumName name of the enumeration type.
     * @param enumValues enumeration values.
     * @param onlyGenerateNested marks a class to be only generated when generating nested content, i.e. when
     *                           generating the body of a data class in which nested classes are placed.
     */
    data class KotlinStringEnumeration(
        val enumName: String,
        val enumValues: List<String>,
        var onlyGenerateNested: Boolean = false
    ) : KotlinEntry()

    /**
     * A OneOf, represented as a sealed class with nested data classes, see [KotlinOneOfParameter].
     *
     * @param oneOfName name of the OneOf.
     * @param onlyGenerateNested marks a class to be only generated when generating nested content, i.e. when
     *                           generating the body of a data class in which nested classes are placed.
     */
    data class KotlinOneOf(
        val oneOfName: String,
        var onlyGenerateNested: Boolean = false
    ) : KotlinEntry()
}