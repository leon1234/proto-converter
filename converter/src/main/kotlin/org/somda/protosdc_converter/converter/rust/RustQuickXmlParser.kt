package org.somda.protosdc_converter.converter.rust

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.EXTENSION_NODE_NAME
import org.somda.protosdc_converter.converter.PARTICIPANT_MODEL_NAMESPACE
import org.somda.protosdc_converter.converter.QUALIFIED_NAME_NODE_NAME
import org.somda.protosdc_converter.converter.requireLegalFolderContents
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.xml.ParserAttributeInfo
import org.somda.protosdc_converter.converter.rust.xml.ChildCollection
import org.somda.protosdc_converter.converter.rust.xml.ParserElementInfo
import org.somda.protosdc_converter.converter.rust.xml.ElementParser
import org.somda.protosdc_converter.converter.rust.xml.OneOfParser
import org.somda.protosdc_converter.converter.rust.xml.RustParser
import org.somda.protosdc_converter.converter.rust.xml.SimpleTypeParser
import org.somda.protosdc_converter.converter.rust.xml.StringEnumParser
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter
import javax.xml.namespace.QName

const val IMPORTS = """use quick_xml::events::{BytesStart, Event};
use protosdc_xml::{GenericXmlReaderSimpleTypeRead, GenericXmlReaderComplexTypeRead};
"""

const val DEFAULT_ALLOWS = "#[allow(unused_assignments, unused_variables, dead_code)]"

const val XSI_TYPE_CONST_NAME = "XSI_TYPE"

fun rustToStringCall(text: String) = "\"$text\".to_string()"

fun boundNamespace(value: String) = "quick_xml::name::ResolveResult::Bound(quick_xml::name::Namespace($value))"
const val unboundNamespace = "quick_xml::name::ResolveResult::Unbound"

const val quickXmlQname = "quick_xml::name::QName"

val DEFINITIONS = """
pub type QName = protosdc_xml::QNameSlice;
    
const XML_SCHEMA_INSTANCE: &[u8] = b"http://www.w3.org/2001/XMLSchema-instance";
const $XSI_TYPE_CONST_NAME: QName = (${boundNamespace("XML_SCHEMA_INSTANCE")}, b"type");

$DEFAULT_ALLOWS
pub fn attribute_to_string(attr: &[u8]) -> String {
    match String::from_utf8(attr.to_vec()) {
        Ok(it) => it,
        Err(_) => ${rustToStringCall("FromUtf8Error")}
    }
}
"""

///
/// Error type used by parser and utility to use it in generator
///
const val PARSER_ERROR = "protosdc_xml::ParserError"
fun parserErrorQuickXmlError(errValue: String) = "$PARSER_ERROR::QuickXMLError($errValue)"
fun parserErrorInvalidAttr(errValue: String) = "quick_xml::Error::InvalidAttr($errValue)"
fun parserErrorParseIntError(errValue: String) = "$PARSER_ERROR::ParseIntError($errValue)"
fun parserErrorOther() = "$PARSER_ERROR::Other" // avoid
fun parserErrorUnexpectedEof() = "$PARSER_ERROR::UnexpectedEof"
fun parserErrorUnexpectedEvent(name: String) = "$PARSER_ERROR::UnexpectedParserEvent { event: $name }"
fun parserErrorUnexpectedParserStartState(element: String, state: String) =
    "$PARSER_ERROR::UnexpectedParserStartState { element_name: $element, parser_state: $state }"

fun parserErrorUnexpectedParserEndState(element: String, state: String) =
    "$PARSER_ERROR::UnexpectedParserEndState { element_name: $element, parser_state: $state }"

fun parserErrorUnexpectedParserTextEventState(state: String) =
    "$PARSER_ERROR::UnexpectedParserTextEventState { parser_state: $state }"

fun parserErrorOneOfParserFallthrough(name: String) = "$PARSER_ERROR::OneOfParserFallthrough { parser_name: $name }"
fun parserErrorMandatoryAttributeMissing(name: String) =
    "$PARSER_ERROR::MandatoryAttributeMissing { attribute_name: $name }"

fun parserErrorMandatoryElementMissing(name: String) = "$PARSER_ERROR::MandatoryElementMissing { element_name: $name }"
fun parserErrorMandatoryContentMissing(name: String) = "$PARSER_ERROR::MandatoryContentMissing { element_name: $name }"

fun parserErrorUnexpectedAttribute(elementName: String, attributeName: String, attributeValue: String) =
    "$PARSER_ERROR::UnexpectedAttribute { element_name: $elementName, attribute_name: $attributeName, attribute_value: $attributeValue }"

fun parserErrorElementError(name: String) = "$PARSER_ERROR::ElementError { element_name: $name }"

fun parserErrorMappingError(name: String) = "$PARSER_ERROR::MappingError($name)"


///
/// Constants and utilities for quick xml
///
const val XML_READER_EXTENSION_GAT = "Extension"
const val XML_READER_TYPE = "protosdc_xml::XmlReader<B, Self::$XML_READER_EXTENSION_GAT>"
const val XML_READER_GENERIC_TYPE = "protosdc_xml::XmlReader<B, C>"
const val XML_READER_INPUT_GENERIC_TYPE = "B: ::std::io::BufRead"
const val XML_READER_EXTENSION_GENERIC_TYPE =
    "C: protosdc_xml::ExtensionType + protosdc_xml::ComplexXmlTypeWrite + 'static"

// these events are boring and not being processed right now
val BORING_EVENTS =
    listOf("Event::Empty(_)", "Event::Comment(_)", "Event::Decl(_)", "Event::PI(_)", "Event::DocType(_)")

const val READER_READ_FUNC = "read_next"

const val READER = "reader"
const val TAG_NAME = "tag_name"
const val EVENT = "event"
const val LOCAL_BUFFER = "buf"

const val COMPLEX_SUFFIX = "_complex"
const val SIMPLE_SUFFIX = "_simple"


///
/// Prebuilt parsers for certain types used by BICEPS
///
const val EXTENSION_PARSER_NAME = "parse_extension_todo_later"
fun redirectExtensionParser(modulePath: String, call: String, genericType: String) = """
${elementParserSignature("$modulePath::Extension", genericType)}
        $call($TAG_NAME, $EVENT, $READER)
    }
}
"""

fun extensionParser(modulePath: String, genericType: String) = """
/// Fast forwards through extension elements, parsing is not implemented yet.
${elementParserSignature("$modulePath::Extension", genericType)}
        let mut $LOCAL_BUFFER = vec![];
        loop {
            match $READER.$READER_READ_FUNC(&mut $LOCAL_BUFFER) {
                // don't care about starts
                Ok((_, Event::Start(_))) => {},
                Ok((ref ns, Event::End(ref e))) => {
                    match (*ns, e.local_name()) {
                        (ns, local_name) if $TAG_NAME.0 == ns && $TAG_NAME.1 == local_name => {
                            return Ok($modulePath::Extension {
                                // TODO XML Parser currently does not support extensions
                                item: vec![],
                            })
                        }
                        // do not care about any other events right now
                        _ => {}
                    }
                },
    ${BORING_EVENTS.joinToString(separator = "\n") { "${INDENT.repeat(3)}Ok((_, $it)) => {}," }}
                Ok((_, Event::Text(_))) => {},
                Ok((_, Event::Eof)) => Err(${parserErrorUnexpectedEof()})?,
                Err(err) => Err(${parserErrorQuickXmlError("err")})?,
            }
        }
    }
}
"""

const val PRIMITIVE_TYPE_STRING_PARSER_NAME = "parse_xsd_string"
val PRIMITIVE_TYPE_STRING_PARSER_QNAME = BuiltinTypes.XSD_STRING.qname
val PRIMITIVE_TYPE_STRING_PARSER = """${primitiveParserSignature(PRIMITIVE_TYPE_STRING_PARSER_NAME, "String")}
    Ok($READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string())
}
"""

const val PRIMITIVE_TYPE_DECIMAL_PARSER_NAME = "parse_xsd_decimal"
val PRIMITIVE_TYPE_DECIMAL_PARSER_QNAME = BuiltinTypes.XSD_DECIMAL.qname
val PRIMITIVE_TYPE_DECIMAL_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_DECIMAL_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDecimal")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    if parsed.is_empty() {
        return Err(${parserErrorOther()})
    }
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_ANY_URI_PARSER_NAME = "parse_xsd_any_uri"
val PRIMITIVE_TYPE_ANY_URI_PARSER_QNAME = BuiltinTypes.XSD_ANY_URI.qname
val PRIMITIVE_TYPE_ANY_URI_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_ANY_URI_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoUri")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""


const val PRIMITIVE_TYPE_LANGUAGE_PARSER_NAME = "parse_xsd_language"
val PRIMITIVE_TYPE_LANGUAGE_PARSER_QNAME = BuiltinTypes.XSD_LANGUAGE.qname
val PRIMITIVE_TYPE_LANGUAGE_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_LANGUAGE_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoLanguage")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_DURATION_PARSER_NAME = "parse_xsd_duration"
val PRIMITIVE_TYPE_DURATION_PARSER_QNAME = BuiltinTypes.XSD_DURATION.qname
val PRIMITIVE_TYPE_DURATION_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_DURATION_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDuration")}
    let as_str = std::str::from_utf8(data)?;
    let parsed_duration = as_str.parse::<iso8601_duration::Duration>()
        .map_err(|it| protosdc_xml::ParserError::ParseDurationError { error: format!("{:?}", it) })?;
    match parsed_duration.to_std() {
        None => Err(protosdc_xml::ParserError::ParseDurationError {
            error: format!("Could not convert to std duration, contains year or month which are ambiguous")
        }),
        Some(it) => Ok(it.into())
    }
}"""

const val PRIMITIVE_TYPE_BOOL_PARSER_NAME = "parse_xsd_bool"
val PRIMITIVE_TYPE_BOOL_PARSER_QNAME = BuiltinTypes.XSD_BOOL.qname
val PRIMITIVE_TYPE_BOOL_PARSER = """${primitiveParserSignature(PRIMITIVE_TYPE_BOOL_PARSER_NAME, "bool")}
    Ok(match data {
        b"0" | b"false" => false,
        b"1" | b"true" => true,
        _ => Err(${parserErrorOther()})?
    })
}"""

const val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER_NAME = "parse_xsd_any_simple_type"
val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER_QNAME = BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname
val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER_NAME, "String")}
    Ok($READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string())
}"""

const val PRIMITIVE_TYPE_DATE_TIME_PARSER_NAME = "parse_xsd_date_time"
val PRIMITIVE_TYPE_DATE_TIME_PARSER_QNAME = BuiltinTypes.XSD_DATE_TIME.qname
val PRIMITIVE_TYPE_DATE_TIME_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_DATE_TIME_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDateTime")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_DATE_PARSER_NAME = "parse_xsd_date"
val PRIMITIVE_TYPE_DATE_PARSER_QNAME = BuiltinTypes.XSD_DATE.qname
val PRIMITIVE_TYPE_DATE_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_DATE_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDate")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_YEAR_MONTH_PARSER_NAME = "parse_xsd_year_month"
val PRIMITIVE_TYPE_YEAR_MONTH_PARSER_QNAME = BuiltinTypes.XSD_G_YEAR_MONTH.qname
val PRIMITIVE_TYPE_YEAR_MONTH_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_YEAR_MONTH_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoYearMonth")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_YEAR_PARSER_NAME = "parse_xsd_year"
val PRIMITIVE_TYPE_YEAR_PARSER_QNAME = BuiltinTypes.XSD_G_YEAR.qname
val PRIMITIVE_TYPE_YEAR_PARSER =
    """${primitiveParserSignature(PRIMITIVE_TYPE_YEAR_PARSER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoYear")}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string();
    Ok(parsed.try_into().map_err(|err| ${parserErrorMappingError("err")})?)
}"""

const val PRIMITIVE_TYPE_QUALIFIED_NAME_PARSER_NAME = "parse_qualified_name"
fun primitiveTypeQualifiedNameParser(modulePath: String) = """${
    primitiveParserSignature(
        PRIMITIVE_TYPE_QUALIFIED_NAME_PARSER_NAME, "$modulePath::QualifiedName"
    )
}
    match $READER.resolve_element($quickXmlQname(data)) {
        (${boundNamespace("ns")}, attr) => {
            Ok($modulePath::QualifiedName {
                namespace: $READER.decoder().decode(ns).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string(),
                local_name: $READER.decoder().decode(attr.into_inner()).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string()
            })
        }
        (_, attr) => {
            Ok($modulePath::QualifiedName {
                namespace: "".to_string(),
                local_name: $READER.decoder().decode(attr.into_inner()).map_err(|err| ${parserErrorQuickXmlError("err")})?.to_string()
            })
        }
    }
}"""


// generate parsers for all integer-style target types we're using
val INTEGER_TYPES_MAP: List<Triple<String, QName, String>> = RustGenerator.BUILT_IN_TYPES
    .filter { it.value.typeName in listOf("i32", "i64", "u32", "u64") }
    .map {
        val parserName = "parse_xsd_" + it.key.localPart.camelToSnakeCase()

        val parserCode =
            """${primitiveParserSignature(parserName, it.value.typeName)}
    let parsed = $READER.decoder().decode(data).map_err(|err| ${parserErrorQuickXmlError("err")})?
        .parse::<${it.value.typeName}>().map_err(|err| ${parserErrorParseIntError("err")})?;
    Ok(parsed)
}"""
        Triple(parserName, it.key, parserCode)
    }.toList()


const val DATE_OF_BIRTH_UNION_PARSER_NAME = "parse_date_of_birth_union"
val DATE_OF_BIRTH_UNION_PARSER_PARENT_QNAME = QName(PARTICIPANT_MODEL_NAMESPACE, "DateOfBirth")
fun dateOfBirthUnionPrimitive(modulePath: String) = """
${
    primitiveParserSignature(
        DATE_OF_BIRTH_UNION_PARSER_NAME,
        "$modulePath::patient_demographics_core_data_mod::DateOfBirth"
    )
}

    let content = $READER
        .decoder()
        .decode(data)
        .map_err(|err| ${parserErrorQuickXmlError("err")})?;

    // try all variants
    let date_time: Result<$MAPPING_TYPES_MODULE_PATH::ProtoDateTime, protosdc_mapping::MappingError> = content.to_string().try_into();
    if let Ok(it) = date_time {
        return Ok($modulePath::patient_demographics_core_data_mod::DateOfBirth::DateTime(it));
    };
    let date: Result<$MAPPING_TYPES_MODULE_PATH::ProtoDate, protosdc_mapping::MappingError> = content.to_string().try_into();
    if let Ok(it) = date {
        return Ok($modulePath::patient_demographics_core_data_mod::DateOfBirth::Date(it));
    };
    let year_month: Result<$MAPPING_TYPES_MODULE_PATH::ProtoYearMonth, protosdc_mapping::MappingError> = content.to_string().try_into();
    if let Ok(it) = year_month {
        return Ok($modulePath::patient_demographics_core_data_mod::DateOfBirth::GYearMonth(it));
    };
    let year: Result<$MAPPING_TYPES_MODULE_PATH::ProtoYear, protosdc_mapping::MappingError> = content.to_string().try_into();
    if let Ok(it) = year {
        return Ok($modulePath::patient_demographics_core_data_mod::DateOfBirth::GYear(it));
    };

    Err(protosdc_xml::ParserError::MappingError(protosdc_mapping::MappingError::FromProtoGeneric { message: "Could not parse date of birth union".to_string() }))
}"""


fun primitiveParserSignature(parserName: String, resultType: String) =
    """${RustQuickXmlParser.allowUnusedAssignment()}
pub fn $parserName<$XML_READER_INPUT_GENERIC_TYPE, $XML_READER_EXTENSION_GENERIC_TYPE>(data: &[u8], $READER: &mut $XML_READER_GENERIC_TYPE) -> Result<$resultType, $PARSER_ERROR> {"""

fun simpleParserSignature(resultType: String, genericType: String) =
    """${RustQuickXmlParser.allowUnusedAssignment()}
impl protosdc_xml::GenericXmlReaderSimpleTypeRead for $resultType {
    type $XML_READER_EXTENSION_GAT = $genericType;
    fn from_xml_simple<$XML_READER_INPUT_GENERIC_TYPE>(data: &[u8], $READER: &mut $XML_READER_TYPE) -> Result<$resultType, $PARSER_ERROR> {
"""

fun elementParserSignature(resultType: String, genericType: String) =
    """${RustQuickXmlParser.allowUnusedAssignment()}
impl protosdc_xml::GenericXmlReaderComplexTypeRead for $resultType {
    type $XML_READER_EXTENSION_GAT = $genericType;
    fn from_xml_complex<$XML_READER_INPUT_GENERIC_TYPE>(tag_name: QName, event: &BytesStart, reader: &mut $XML_READER_TYPE) -> Result<Self, $PARSER_ERROR> {
"""


fun primitiveParserCall(parserName: String, variable: String) = "$parserName($variable, $READER)"

fun simpleParserCall(typeName: String, variable: String) = "$typeName::from_xml_simple($variable, $READER)"

fun elementParserCall(typeName: String, tagName: String, event: String) =
    "$typeName::from_xml_complex($tagName, $event, $READER)"

@kotlinx.serialization.Serializable
data class QuickXmlParserConfig(
    val rustModule: String,
    val outputFolder: String = "rust_quick_xml/",
    val outputFile: String = "parser.rs",
    val modelRustModuleSubstitute: String? = null,
    val extensionParserCall: CallOverride? = null,
    val extensionAssociatedType: String = "Box<dyn $MAPPING_TYPES_MODULE_PATH::any::AnyContent + Send + Sync>"
) {
    init {
        require(!rustModule.contains("."))
        require(outputFolder.endsWith("/"))
        require(outputFile.endsWith(".$RUST_FILE_EXTENSION"))
        require(!(modelRustModuleSubstitute?.endsWith("::") ?: false))
    }
}

class RustQuickXmlParser(
    private val tree: BaseNode,
    private val converterConfig: ConverterConfig,
    private val rustConfig: RustConfig,
    private val config: QuickXmlParserConfig,
    private val generateCustomTypes: Boolean = true,
) : BaseNodePlugin {

    companion object : Logging {
        private const val TOML_TABLE_NAME = "QuickXmlParser"

        const val ROOT_STATE = "Root"
        private const val START_SUFFIX = "Start"
        private const val END_SUFFIX = "End"

        const val CONTENT_FIELD_NAME = "content"

        fun toStart(name: String) = "$name$START_SUFFIX"
        fun toEnd(name: String) = "$name$END_SUFFIX"

        fun allowUnusedAssignment() = DEFAULT_ALLOWS

        // collect ancestors, ordered from oldest to youngest
        fun collectAncestors(node: BaseNode): List<BaseNode> {
            val ancestors: List<BaseNode> = when (val nestedNodeType = node.nodeType) {
                is NodeType.Message -> nestedNodeType.extensionBaseNode?.let { collectAncestors(it) } ?: emptyList()
                else -> emptyList()
            }
            return ancestors + listOf(node)
        }

        private val parserForbiddenNames = mapOf(
            "state" to "state_x"
        )

        fun validateName(name: String): String {
            return RustGenerator.FORBIDDEN_PARAMETER_NAMES[name] ?: parserForbiddenNames[name] ?: name
        }


        fun createBox(element: String) = "::std::boxed::Box::new($element)"

        fun isOptionalContent(node: BaseNode) = when (val content = node.nodeType) {
            is NodeType.Parameter -> {
                when {
                    // enum type is referenced, check if empty is allowed
                    content.parameterType.parent != null && isEnumContainer(content.parameterType.parent!!) -> {
                        // ensure empty string is not allowed value
                        val enumType = content.parameterType.parent!!.children[0].nodeType as NodeType.StringEnumeration
                        enumType.values.contains("")
                    }

                    else -> {
                        // check if type references a type that allows empty
                        content.parameterType.parent?.children?.size == 1 &&
                            when (val nested = content.parameterType.parent!!.children[0].nodeType) {
                                is NodeType.Parameter -> run {
                                    val minApplies = minLengthApplicable(nested.parentNode!!)
                                    val minLengthIsZero = (nested.restriction?.minLength ?: 0) == 0
                                    minApplies && minLengthIsZero
                                }

                                else ->
                                    // check if the parameter type referenced directly allows empty
                                    minLengthApplicable(node) &&
                                        (content.restriction?.minLength ?: 0) == 0
                            }
                    }
                }
            }

            else -> false
        }


        fun parseConfig(config: String): QuickXmlParserConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, TOML_TABLE_NAME)
        }
    }

    // track all generated parsers by name, api is defined by being simple or complex
    private val primitiveParsers: MutableMap<BaseNode, RustParser> = mutableMapOf()
    private val simpleParsers: MutableMap<BaseNode, RustParser> = mutableMapOf()
    private val complexParsers: MutableMap<BaseNode, RustParser> = mutableMapOf()

    private val builtinParsers: List<Triple<String, QName, String>> = listOf(
        Triple(PRIMITIVE_TYPE_STRING_PARSER_NAME, PRIMITIVE_TYPE_STRING_PARSER_QNAME, PRIMITIVE_TYPE_STRING_PARSER),
        Triple(PRIMITIVE_TYPE_DECIMAL_PARSER_NAME, PRIMITIVE_TYPE_DECIMAL_PARSER_QNAME, PRIMITIVE_TYPE_DECIMAL_PARSER),
        Triple(PRIMITIVE_TYPE_ANY_URI_PARSER_NAME, PRIMITIVE_TYPE_ANY_URI_PARSER_QNAME, PRIMITIVE_TYPE_ANY_URI_PARSER),
        Triple(
            PRIMITIVE_TYPE_LANGUAGE_PARSER_NAME,
            PRIMITIVE_TYPE_LANGUAGE_PARSER_QNAME,
            PRIMITIVE_TYPE_LANGUAGE_PARSER
        ),
        Triple(
            PRIMITIVE_TYPE_DURATION_PARSER_NAME,
            PRIMITIVE_TYPE_DURATION_PARSER_QNAME,
            PRIMITIVE_TYPE_DURATION_PARSER
        ),
        Triple(PRIMITIVE_TYPE_BOOL_PARSER_NAME, PRIMITIVE_TYPE_BOOL_PARSER_QNAME, PRIMITIVE_TYPE_BOOL_PARSER),
        Triple(
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER_NAME,
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER_QNAME,
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_PARSER
        ),
        Triple(
            PRIMITIVE_TYPE_DATE_TIME_PARSER_NAME,
            PRIMITIVE_TYPE_DATE_TIME_PARSER_QNAME,
            PRIMITIVE_TYPE_DATE_TIME_PARSER
        ),
        Triple(PRIMITIVE_TYPE_DATE_PARSER_NAME, PRIMITIVE_TYPE_DATE_PARSER_QNAME, PRIMITIVE_TYPE_DATE_PARSER),
        Triple(
            PRIMITIVE_TYPE_YEAR_MONTH_PARSER_NAME,
            PRIMITIVE_TYPE_YEAR_MONTH_PARSER_QNAME,
            PRIMITIVE_TYPE_YEAR_MONTH_PARSER
        ),
        Triple(PRIMITIVE_TYPE_YEAR_PARSER_NAME, PRIMITIVE_TYPE_YEAR_PARSER_QNAME, PRIMITIVE_TYPE_YEAR_PARSER),
    ) + INTEGER_TYPES_MAP

    init {

        // attach builtin parsers
        builtinParsers.forEach { (parserName, qname, _) ->
            val node = tree.findQName(qname = qname)!!
            primitiveParsers[node] = parserName
        }

        if (generateCustomTypes) {
            // insert qualified name parser
            val qualifiedNameNode = tree.children
                .filter { it.isCustomType }
                .find { it.nodeName == QUALIFIED_NAME_NODE_NAME }!!
            primitiveParsers[qualifiedNameNode] = PRIMITIVE_TYPE_QUALIFIED_NAME_PARSER_NAME

            // insert union parser
            tree.findQName(DATE_OF_BIRTH_UNION_PARSER_PARENT_QNAME)?.let { dobParent ->
                val dobNode = dobParent.children[0]
                check(dobNode.nodeType is NodeType.OneOf)

                primitiveParsers[dobNode] = DATE_OF_BIRTH_UNION_PARSER_NAME
            } ?: run { logger.warn("Could not find Date of Birth Union to attach to") }

            // nop extension type for now
            logger.warn("NOP-ing extension type, fixme")
            val extensionTypeNode = tree.findQName(QName(Constants.EXT_NAMESPACE, "ExtensionType"))!!
            complexParsers[extensionTypeNode] = "parse_extension_type_todo_later"

            val extensionNode = tree.children
                .filter { it.isCustomType }
                .find {
                    it.nodeName == EXTENSION_NODE_NAME
                }!!
            complexParsers[extensionNode] = EXTENSION_PARSER_NAME
        }

        tree.children.forEach { child ->
            when (val nodeType = child.nodeType) {
                is NodeType.Message -> {
                    nodeType.episodePluginData[pluginIdentifier()]?.let {
                        when (val episodeData = it) {
                            is EpisodeData.Mapper -> when (episodeData.sourceToTargetFunction) {
                                "simple" -> {
                                    simpleParsers[child] = episodeData.sourceTypeName
                                }

                                "complex" -> {
                                    complexParsers[child] = episodeData.sourceTypeName
                                }

                                else -> {
                                    // nop
                                }
                            }

                            else -> {
                                // nop
                            }
                        }
                    }
                }

                else -> {
                    // nop
                }
            }
        }
    }

    private fun processNode(node: BaseNode, parentNode: BaseNode?, target: PrintWriter) {

        logger.info("Processing $node")

        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")

            if (node.clusterHandled[OutputLanguage.RustQuickXMLParser] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            val totalCluster = listOf(node) + cluster

            totalCluster.forEach { clusterNode ->

                val parserName = "parse_${clusterNode.nodeName.camelToSnakeCase()}"

                // simply inject complex type parsers, let's see what happens
                complexParsers[clusterNode] = parserName + COMPLEX_SUFFIX
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.RustQuickXMLParser] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }

        parentNode?.let {
            check(it.nodeType is NodeType.Message) { "Only message parents are supported" }
        }


        // special case handling here
        if (node.isCustomType) {
            when (node.nodeName) {
                QUALIFIED_NAME_NODE_NAME, EXTENSION_NODE_NAME -> {
                    // TODO
                    return
                }

                else -> TODO()
            }
        }

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {

                // we do not care about messages below the root without qname, might be something custom or whatever
                if (nodeType.qname == null && parentNode == null) {
                    logger.debug("node below root without qname: $node")
                }

                val parserNamePrefix = "parse_"

                val parserName: String = when (parentNode) {
                    null -> {
                        // simple type under root
                        "$parserNamePrefix${node.nodeName.camelToSnakeCase()}"
                    }

                    else -> {

                        val parentThing = parentNode.nodeName.camelToSnakeCase()
                        when (node.languageType[OutputLanguage.Rust] is RustEntry.RustStruct) {
                            true -> {
                                val rustStruct = node.languageType[OutputLanguage.Rust] as RustEntry.RustStruct
                                val middleSegment = rustStruct.modulePath?.replace(MOD_SEP, "_") ?: parentThing
                                    .replacePrefixWithCrate(config.modelRustModuleSubstitute)
                                "$parserNamePrefix${middleSegment}_${node.nodeName.camelToSnakeCase()}"
                            }

                            false -> "$parserNamePrefix${parentNode.nodeName.camelToSnakeCase()}_${node.nodeName.camelToSnakeCase()}"
                        }

                    }
                }

                val hasEnumChildren = node.children.size == 2 && node.children[0].nodeType is NodeType.StringEnumeration

                // detect embedded enum types and generate a parser for them
                when (node.originalXmlNode?.xmlType) {
                    XmlType.SimpleType, XmlType.Attribute, is XmlType.Element -> {
                        if (hasEnumChildren) {
                            val sep = StringEnumParser(
                                parserName = parserName + SIMPLE_SUFFIX,
                                dataType = node,
                                rustModulePath = rustConfig.rustModule,
                            )

                            val generated = sep.generateParser(config, primitiveParsers, simpleParsers, complexParsers)

                            simpleParsers[node] = sep.parserName

                            target.println(generated)
                            logger.debug(generated)
                        }
                    }

                    else -> Unit
                }

                when (node.originalXmlNode?.xmlType) {
                    XmlType.SimpleType, XmlType.Attribute -> {

                        // do not care if this is already done
                        if (simpleParsers.containsKey(node)) {
                            logger.warn("simple node $node already parsed")
                            return
                        }

                        when (hasEnumChildren) {
                            true -> {
                                // already processed
                            }

                            else -> {

                                // this message is a container for one parameter
                                check(node.children.size == 1)
                                val parameter = node.children[0]
                                val parameterNodeType = parameter.nodeType as NodeType.Parameter

                                // find parser for the primitive type we're using
                                val parameterType = checkNotNull(parameterNodeType.parameterType.parent) {
                                    "parameter type for node $node was null, wat?"
                                }

                                val stp = SimpleTypeParser(
                                    parserName = parserName + SIMPLE_SUFFIX,
                                    dataType = node,
                                    rustModulePath = rustConfig.rustModule,
                                    primitiveParser = primitiveParsers[parameterType],
                                    simpleParser = simpleParsers[parameterType],
                                )

                                val generated =
                                    stp.generateParser(config, primitiveParsers, simpleParsers, complexParsers)

                                simpleParsers[node] = stp.parserName

                                target.println(generated)
                                logger.debug(generated)
                            }
                        }

                    }

                    XmlType.ComplexType, is XmlType.Element -> {

                        // do not care if this is already done
                        if (complexParsers.containsKey(node) && node.clusteredTypes == null) {
                            logger.warn("complex node $node already parsed")
                            return
                        }

                        // create parser for each nested type
                        val nestedMessages = node.children
                            .filter { it.nodeType is NodeType.Message }

                        nestedMessages.forEach { processNode(it, node, target) }

                        // find all types we're inheriting from
                        val ancestors = nodeType.extensionBaseNode?.let { collectAncestors(it) } ?: emptyList()
                        logger.info { "Ancestors for $node are $ancestors" }

                        // collect attribute parameters
                        val attributes = (ancestors + listOf(node)).flatMap {
                            it.children.filter { it.nodeType is NodeType.Parameter && (it.nodeType as NodeType.Parameter).wasAttribute }
                        }.toList()

                        val mappedAttributes = attributes
                            .flatMap {
                                val nodeParameterType = (it.nodeType as NodeType.Parameter)

                                when (nodeParameterType.parameterType.parent?.originalXmlNode?.xmlType == XmlType.AttributeGroup) {
                                    // flatten
                                    true -> nodeParameterType.parameterType.parent!!.children
                                    else -> listOf(it)
                                }
                            }
                            .map {
                                val nodeParameterType = (it.nodeType as NodeType.Parameter)
                                val parent =
                                    checkNotNull(nodeParameterType.parameterType.parent) { "parent for parameter type missing" }
//                                val attributeTypeParser = checkNotNull(simpleParsers[parent]) {
//                                    "simple type parser for $parent missing"
//                                }
                                val parameterRustType =
                                    nodeParameterType.parameterType.parent!!.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct
                                val fullyQualifiedRustTypeName =
                                    parameterRustType.fullyQualifiedName(config.modelRustModuleSubstitute)

                                ParserAttributeInfo(
                                    attributeQName = it.originalXmlNode!!.qName!!,
                                    attributeTypePrimitiveParser = primitiveParsers[parent],
                                    attributeTypeSimpleParser = simpleParsers[parent],
                                    attributeType = nodeParameterType.parameterType.parent!!,
                                    isOptional = nodeParameterType.optional,
                                    isList = nodeParameterType.list,
                                    attributeOriginNode = it,
                                    rustTypeName = fullyQualifiedRustTypeName,
                                )
                            }

                        // determine if this element has content, i.e. if it extends a simple type or has simple content with an extension
                        val contentData: Triple<BaseNode, BaseNode, BaseNode>? = ancestors.find {
                            findContentTypeNode(it) != null
                        }?.let {
                            val (parameterNode, contentType) = findContentTypeNode(it)!!
                            Triple(it, parameterNode, contentType)
                        }
                            ?: findContentTypeNode(node)?.let { Triple(node, it.first, it.second) }
                            ?: run {
                                if (isUnionContainer(node)) {
                                    Triple(node, node.children[0], node.children[0])
                                } else {
                                    null
                                }
                            }
                            ?: run {
                                if (isEnumContainer(node)) {
                                    // the simple type parser also generated is our content type
                                    Triple(node, node, node) // TODO: unsure about (node, node, ..)
                                } else {
                                    null
                                }
                            }

                        // determine if this element is only an element with a type
                        val typeNode: BaseNode? = getElementType(node)
                        typeNode?.let { logger.debug { "node ${node.nodeName} has nodeType $typeNode" } }

                        // determine if this element is a container for a choice (which is the only supported kind of choice)
                        val isChoice: Boolean = node.children.size == 1 && node.children[0].originalXmlNode?.xmlType is XmlType.Choice

                        val choiceChildCollection: ChildCollection?
                        if (isChoice) {
                            // get all variants
                            val variants = node.children[0].children.map { baseNode ->
                                val paramNode = baseNode.nodeType as NodeType.Parameter
                                val typeQName = paramNode.parameterType.parent?.originalXmlNode?.qName

                                ParserElementInfo(
                                    elementStepName = node.nodeName + baseNode.nodeName,
                                    elementQName = baseNode.originalXmlNode?.qName ?: checkNotNull(typeQName) {
                                        "Both xml nodename and typeQName were null for ${baseNode.nodeName}"
                                    },
                                    elementTypeQName = typeQName,
                                    // attribute are handled by nested parser
                                    attributes = emptyList(),
                                    isOptional = paramNode.optional,
                                    isList = paramNode.list,
                                    elementNode = baseNode,
                                    contentParserPrimitive = primitiveParsers[paramNode.parameterType.parent!!],
                                    contentParserSimple = simpleParsers[paramNode.parameterType.parent!!],
                                    elementType = null,
                                    elementTypeParser = null,
                                )
                            }

                            choiceChildCollection = ChildCollection.Choice(variants)
                        } else {
                            choiceChildCollection = null
                        }


                        val elements = (ancestors + listOf(node)).map { baseNode ->
                            baseNode.children
                                // filter attributes which are definitely not children
                                .filter {
                                    when (it.nodeType is NodeType.Parameter) {
                                        true -> !(it.nodeType as NodeType.Parameter).wasAttribute
                                        // only parameters pls
                                        false -> false
                                    }
                                }
                                // filter children from simple types
                                .filter {
                                    baseNode.originalXmlNode?.xmlType !is XmlType.SimpleType
                                }
                                // filter children from complex types that are simple content extension types
                                .filter {
                                    !((baseNode.originalXmlNode?.xmlType is XmlType.ComplexType || baseNode.originalXmlNode?.xmlType is XmlType.Element) && it.originalXmlNode?.xmlType is XmlType.SimpleContentExtension)
                                }
                                // filter children from restrictions, which are just simple types in disguise
                                .filter {
                                    baseNode.originalXmlNode?.xmlType !is XmlType.Restriction
                                }
                                // filter children which are containers for anonymous nested simple types because jesus fucking christ that's such a good idea
                                .filter {
                                    when (it.nodeType is NodeType.Parameter) {
                                        true -> (it.nodeType as NodeType.Parameter).parameterType.parent?.let { parent -> parent.originalXmlNode?.let { node -> node.qName != null } }
                                            ?: true

                                        false -> true
                                    }
                                }
                        }
                            .filter { it.isNotEmpty() }
                            .map {
                                // assume that all of them are sequences if they are not choices
                                ChildCollection.Sequence(it.map { baseNode ->

                                    val paramNode = baseNode.nodeType as NodeType.Parameter
                                    val typeQName = paramNode.parameterType.parent?.originalXmlNode?.qName

                                    ParserElementInfo(
                                        elementStepName = node.nodeName + baseNode.nodeName,
                                        elementQName = baseNode.originalXmlNode?.qName ?: checkNotNull(typeQName) {
                                            "Both xml nodename and typeQName were null for ${baseNode.nodeName}"
                                        },
                                        elementTypeQName = typeQName ?: baseNode.originalXmlNode?.qName,
                                        // attribute are handled by nested parser
                                        attributes = emptyList(),
                                        isOptional = paramNode.optional,
                                        isList = paramNode.list,
                                        elementNode = baseNode,
                                        contentParserPrimitive = primitiveParsers[paramNode.parameterType.parent!!],
                                        contentParserSimple = simpleParsers[paramNode.parameterType.parent!!],
                                        elementType = null,
                                        elementTypeParser = null,
                                    )
                                }.toList())
                            }
                            .toList()

                        contentData?.let {
                            // find parser for content
                            check(simpleParsers[it.third] != null || primitiveParsers[it.third] != null)
                            {
                                "Could not find parser for content node $it for $node"
                            }
                        }

                        val element = ParserElementInfo(
                            elementStepName = node.nodeName,
                            elementQName = node.originalXmlNode!!.qName!!,
                            elementTypeQName = node.originalXmlNode!!.qName!!,
                            elementType = typeNode,
                            elementTypeParser = complexParsers[typeNode],
                            attributes = mappedAttributes,
                            isOptional = false,
                            isList = false,
                            elementNode = node,
                            ancestorTypes = ancestors,
                            children = choiceChildCollection?.let { listOf(it) + elements } ?: elements,
                            contentParserPrimitive = primitiveParsers[contentData?.third],
                            contentParserSimple = simpleParsers[contentData?.third],
                            contentType = contentData?.third,
                        )

                        val parser = ElementParser(
                            parserName = parserName + COMPLEX_SUFFIX,
                            dataType = node,
                            element = element,
                            rustModulePath = rustConfig.rustModule
                        )

                        val parserGenerated =
                            parser.generateParser(config, primitiveParsers, simpleParsers, complexParsers)

                        target.println(parserGenerated)
                        logger.debug(parserGenerated)

                        complexParsers[node] = parser.parserName

//                        TODO("Handle complexType $node")
                    }

                    XmlType.AttributeGroup -> {
                        // NOP
                    }

                    null -> {
                        // check if it is a custom oneof
                        val isOneOf = node.children.size == 1 && node.children[0].nodeType is NodeType.OneOf

                        if (!isOneOf) {
                            TODO("Handle $node")
                        }
                        logger.info("Handling OneOf node $node")

                        val parser = OneOfParser(
                            parserName = parserName,
                            dataType = node,
                            rustModulePath = rustConfig.rustModule
                        )

                        val parserGenerated =
                            parser.generateParser(config, primitiveParsers, simpleParsers, complexParsers)
                        target.println(parserGenerated)
                        logger.debug(parserGenerated)
                        complexParsers[node] = parserName

                    }

                    else -> {
                        // ignore MustUnderstand here
                        if (node.originalXmlNode?.qName != null && node.originalXmlNode!!.qName!!.localPart == "MustUnderstand") {
                            return
                        }

                        TODO("Handle ${node.originalXmlNode}")
                    }
                }

            }

            is NodeType.BuiltinType -> TODO()
            is NodeType.OneOf -> TODO()
            is NodeType.Parameter -> TODO()
            NodeType.Root -> TODO()
            is NodeType.StringEnumeration -> TODO()
            null -> TODO()
        }


    }

    override fun run() {

        val outputFolder = File(converterConfig.outputFolder, config.outputFolder)
        requireLegalFolderContents(outputFolder, RUST_FILE_EXTENSION)
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()

        val outputFile = File(outputFolder, config.outputFile)

        val os = ByteArrayOutputStream()
        PrintWriter(os).use { printWriter ->

            printWriter.println(IMPORTS)
            printWriter.println(DEFINITIONS)

            if (generateCustomTypes) {
                printWriter.println(primitiveTypeQualifiedNameParser(rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute)))

                config.extensionParserCall?.let {
                    printWriter.println(
                        redirectExtensionParser(
                            rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute),
                            it.fullyQualified(config.modelRustModuleSubstitute),
                            config.extensionAssociatedType
                        )
                    )
                } ?: run {
                    printWriter.println(
                        extensionParser(
                            rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute),
                            config.extensionAssociatedType
                        )
                    )
                }
                printWriter.println(dateOfBirthUnionPrimitive(rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute)))
            }

            builtinParsers.forEach { (_, _, parserCode) ->
                printWriter.println(parserCode)
            }

            filterTreeElements(tree, generateCustomTypes)
                .filter {
                    it.languageType[OutputLanguage.Rust]?.let { rs ->
                        when (val rsc = rs) {
                            is RustEntry.RustStruct -> !rsc.customType
                            else -> true
                        }
                    } ?: true
                }
                .forEach { node ->
                    processNode(node, null, printWriter)
                }
        }

        outputFile.writeText(os.toString())
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        return when {
            this.simpleParsers[node] != null -> {
                val parser = this.simpleParsers[node]!!
                EpisodeData.Mapper(
                    packageName = node.nodeName,
                    sourceToTargetFunction = "simple",
                    sourceStructName = "",
                    sourceTypeName = parser,
                    targetStructName = "",
                    targetToSourceFunction = "",
                    targetTypeName = ""
                )
            }

            this.complexParsers[node] != null -> {
                val parser = this.complexParsers[node]!!
                EpisodeData.Mapper(
                    packageName = node.nodeName,
                    sourceToTargetFunction = "complex",
                    sourceStructName = "",
                    sourceTypeName = parser,
                    targetStructName = "",
                    targetToSourceFunction = "",
                    targetTypeName = ""
                )
            }

            else -> EpisodeData.Model(packageName = "", typeName = "")
        }
    }

    override fun pluginIdentifier(): String {
        return "QuickXmlParserV1"
    }

}