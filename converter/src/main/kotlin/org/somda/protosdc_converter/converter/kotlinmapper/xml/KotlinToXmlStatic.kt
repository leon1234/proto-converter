package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType

/**
 * Allows for usage of prefixes when data is marshalled.
 */
object NamespaceMapping {
    operator fun invoke() = listOf(
        CodeLine("private val namespaceMapping: MutableMap<String, String> = mutableMapOf()"),
        CodeLine(""),
        CodeLine("fun $FUNCTION_NAME_REGISTER(namespacesToPrefixes: Map<String, String>) {"),
        CodeLine(1, "namespaceMapping.putAll(namespacesToPrefixes)"),
        CodeLine("}"),
        CodeLine("private fun $FUNCTION_NAME_APPLY_PREFIX(node: ${KotlinType.DOM_NODE}, namespace: String?) {"),
        CodeLine(1, "if (namespace == null) {"),
        CodeLine(2, "return"),
        CodeLine(1, "}"),
        CodeLine(1, "namespaceMapping[namespace]?.also { node.prefix = it }"),
        CodeLine("}"),
    )

    private const val FUNCTION_NAME_REGISTER = "registerNamespaceMapping"
    const val FUNCTION_NAME_APPLY_PREFIX = "applyPrefix"
}