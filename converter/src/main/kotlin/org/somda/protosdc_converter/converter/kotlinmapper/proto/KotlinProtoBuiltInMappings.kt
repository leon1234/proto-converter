package org.somda.protosdc_converter.converter.kotlinmapper.proto

import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlinmapper.FullMappingDefinition

/**
 * Interface for custom Kotlin <-> Proto mappings.
 */
interface ProtoMapping {
    /**
     * Returns mapper metadata from Proto to Kotlin.
     */
    fun fromProto(): FullMappingDefinition

    /**
     * Returns mapper metadata from Kotlin to Proto.
     */
    fun toProto(): FullMappingDefinition
}

private const val PROTO_DECIMAL = "org.somda.protosdc.proto.model.common.Decimal"
private const val JAVA_DECIMAL = "java.math.BigDecimal"

private const val PROTO_DECIMAL_TO_BIG_DECIMAL_NAME: String = "mapProtoDecimalToBigDecimal"
private val protoDecimalToBigDecimalCode = listOf(
    CodeLine("fun $PROTO_DECIMAL_TO_BIG_DECIMAL_NAME(value: $PROTO_DECIMAL): $JAVA_DECIMAL {"),
    CodeLine(1, "return $JAVA_DECIMAL.valueOf(value.value, value.scale)"),
    CodeLine("}")
)

private const val BIG_DECIMAL_TO_PROTO_DECIMAL_NAME: String = "mapBigDecimalToProtoDecimal"
private val bigDecimalToProtoDecimalCode = listOf(
    CodeLine("fun $BIG_DECIMAL_TO_PROTO_DECIMAL_NAME(value: $JAVA_DECIMAL): $PROTO_DECIMAL {"),
    CodeLine(1, "return $PROTO_DECIMAL.newBuilder()"),
    CodeLine(2, ".setValue(value.unscaledValue().longValueExact())"),
    CodeLine(2, ".setScale(value.scale())"),
    CodeLine(2, ".build()"),
    CodeLine("}")
)

/**
 * Predefined custom mapping of decimals.
 */
object DecimalMapping : ProtoMapping {
    override fun fromProto() = FullMappingDefinition(
        PROTO_DECIMAL,
        JAVA_DECIMAL,
        PROTO_DECIMAL_TO_BIG_DECIMAL_NAME,
        protoDecimalToBigDecimalCode
    )

    override fun toProto() = FullMappingDefinition(
        JAVA_DECIMAL,
        PROTO_DECIMAL,
        BIG_DECIMAL_TO_PROTO_DECIMAL_NAME,
        bigDecimalToProtoDecimalCode
    )
}