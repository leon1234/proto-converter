package org.somda.protosdc_converter.converter

import com.akuleshov7.ktoml.Toml
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.file
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.kotlin.*
import org.somda.protosdc_converter.converter.kotlinmapper.proto.DecimalMapping
import org.somda.protosdc_converter.converter.kotlinmapper.proto.KotlinProtoExtensionsPlugin
import org.somda.protosdc_converter.converter.kotlinmapper.proto.KotlinProtoMappingPlugin
import org.somda.protosdc_converter.converter.kotlinmapper.xml.*
import org.somda.protosdc_converter.converter.rust.RustBase
import org.somda.protosdc_converter.converter.rust.RustGenerator
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser
import org.somda.protosdc_converter.converter.rust.RustQuickXmlWriter
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.File
import javax.xml.namespace.QName


fun isOneOfContainer(it: BaseNode) = it.children.size == 1 && it.children[0].nodeType is NodeType.OneOf

fun processOneOneOfToEpisode(it: BaseNode, plugins: List<BaseNodePlugin>): Main.PrebuiltIdentifier.OneOfIdentified {
    check(isOneOfContainer(it))
    val parameters = it.children.first().children
    check(parameters.find { it.nodeType is NodeType.OneOf } == null)

    val parameterInfo: List<Pair<String, Main.SerializableQName>> = parameters
        .map { Pair(it, it.nodeType as NodeType.Parameter) }
        .map { (node, nodeType) -> Pair(node, nodeType.parameterType as NodeType.Message) }
        .map { (node, msg) -> node.nodeName to Main.SerializableQName.fromQName(msg.qname!!) }
        .toList()

    val pluginData = plugins.associate { plugin ->
        plugin.pluginIdentifier() to plugin.episodeData(it)
    }

    return Main.PrebuiltIdentifier.OneOfIdentified(pluginData, it.nodeName, parameterInfo)
}

@Serializable
data class ConverterConfig(
    val schemaFile: String,
    val loopThreshold: Long,
    val episodeLocationIn: String? = null,
    val episodeLocationOut: String? = null,
    val attributeSuffix: String,
    val outputFolder: String = "generated/"
) {
    init {
        require(outputFolder.endsWith("/"))
    }
}


fun main(args: Array<String>) = Main().main(args)

class Main : CliktCommand(name = "protosdc-converter") {

    companion object : Logging

    private val configFile by option("--config", "-c", help = "TOML config file")
        .file()
        .required()

    override fun run() {
        Configurator.setRootLevel(Level.INFO)

        val tomlContent = configFile.readText()

        val converterConfig =
            Toml.partiallyDecodeFromString<ConverterConfig>(serializer(), tomlContent, "Converter.General")

        val dependencyTree =
            XmlSchemaPostProcessor(File(converterConfig.schemaFile), converterConfig.loopThreshold.toInt()).resolve()

        val episodeIn: List<PrebuiltIdentifier>? =
            converterConfig.episodeLocationIn?.let { Json.decodeFromString<List<PrebuiltIdentifier>>(File(it).readText()) }
        episodeIn?.let {
            logger.info("Loaded ${it.size} episode entries")
        }

        val baseTypeNodes = BaseTypeProcessor(dependencyTree, converterConfig.attributeSuffix).generate().also {
            it.validateTree()
        }

        logger.info("Types to be generated: ${baseTypeNodes.children.size}")
        logger.debug { "Types to be generated (concrete): ${baseTypeNodes.children.joinToString { it.nodeName }}" }

        // attach custom types into the base types
        CustomTypesEnricher(baseTypeNodes).enrich()
        baseTypeNodes.validateTree()

        val protoConfig =
            Toml.partiallyDecodeFromString<ProtoConfig>(serializer(), tomlContent, Proto3Generator.TOML_TABLE_NAME)
        val kotlinConfig = KotlinModelPlugin.parseConfig(tomlContent)
        val kotlinProtoMappingConfig = KotlinProtoMappingPlugin.parseConfig(tomlContent)
        val kotlinXmlMappingConfig = KotlinXmlMappingPlugin.parseConfig(tomlContent)
        val kotlinProtoExtensionsConfig = KotlinProtoExtensionsPlugin.parseConfig(tomlContent)
        val kotlinXmlExtensionsConfig = KotlinXmlExtensionsPlugin.parseConfig(tomlContent)
        val rustMapperConfig = RustGenerator.parseConfig(tomlContent)
        val quickXmlParserConfig = RustQuickXmlParser.parseConfig(tomlContent)
        val quickXmlWriterConfig = RustQuickXmlWriter.parseConfig(tomlContent)

        // process episodes and mark types as skip
        processEpisodeIn(episodeIn, baseTypeNodes)

        // process prebuilt info
        val protoGen = Proto3Generator(
            tree = baseTypeNodes,
            protoConfig = protoConfig,
            generateCustomTypes = episodeIn == null,
            converterConfig = converterConfig,
        )
        protoGen.run()

        // remove kotlin source files from generated folder
        cleanUpOutputFolders(
            converterConfig.outputFolder,
            kotlinConfig.outputFolders +
                    kotlinConfig.outputFoldersNoHierarchy +
                    kotlinProtoMappingConfig.outputFolders +
                    kotlinProtoMappingConfig.outputFoldersNoHierarchy +
                    kotlinProtoExtensionsConfig.outputFolders +
                    kotlinProtoExtensionsConfig.outputFoldersNoHierarchy +
                    kotlinXmlMappingConfig.outputFoldersNoHierarchy +
                    kotlinXmlMappingConfig.outputFolders
        )

        val kotlinModel = KotlinModelPlugin(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            kotlinConfig = kotlinConfig,
            generateCustomTypes = episodeIn == null,
            // ext:ExtensionType maps to kotlin.Any
            customTypes = object : KotlinTypeMap {
                private val customTypes = mapOf(
                    QName(Constants.EXT_NAMESPACE, "ExtensionType") to KotlinEntry.DataClass("Any")
                )

                override fun invoke(): Map<QName, KotlinEntry.DataClass> = customTypes
                override operator fun get(type: QName): KotlinEntry.DataClass? = customTypes[type]
            }
        ).apply { run() }

        val kotlinProtoMapper = KotlinProtoMappingPlugin(
            tree = baseTypeNodes,
            protoCustomTypesTree = protoGen.optionalCompanionsRoot,
            converterConfig = converterConfig,
            kotlinConfig = kotlinConfig,
            kotlinProtoMappingConfig = kotlinProtoMappingConfig,
            customMappers = listOf(DecimalMapping),
            generateCustomTypes = episodeIn == null
        )
        kotlinProtoMapper.run()

        // biceps's retrievability element needs to be added as custom extension node
        // as it lacks a mustUnderstand attribute
        val customExtensionNodes = baseTypeNodes.children.filter {
            (it.nodeType as? NodeType.Message)?.qname == Constants.msgQName(
                "Retrievability"
            )
        }

        KotlinProtoExtensionsPlugin(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            kotlinConfig = kotlinConfig,
            kotlinProtoMappingConfig = kotlinProtoMappingConfig,
            kotlinExtensionsConfig = kotlinProtoExtensionsConfig,
            customExtensionNodes = customExtensionNodes,
            mapperUtil = kotlinProtoMapper
        ).run()

        val kotlinXmlMapper = KotlinXmlMappingPlugin(
            tree = baseTypeNodes,
            protoCustomTypesTree = protoGen.optionalCompanionsRoot,
            converterConfig = converterConfig,
            kotlinConfig = kotlinConfig,
            kotlinXmlMappingConfig = kotlinXmlMappingConfig,
            customMappers = mutableListOf<XmlMapping>().apply {
                // extension mapper is only required for biceps extension namespace
                if (!qNameInEpisode(episodeIn, Constants.extQName("Extension"))) {
                    add(ExtensionItemMapping(kotlinConfig.kotlinPackage))
                }
                // the qualified name is supposed to be a standalone serializable class independent of biceps
                // however, it is currently part of the biceps model and hence included when compiling biceps
                if (!qNameInEpisode(episodeIn, Constants.xsdQName("QName"))) {
                    add(QNameMapping(kotlinConfig.kotlinPackage))
                }
            },
            generateCustomTypes = episodeIn == null
        )
        kotlinXmlMapper.run()

        KotlinXmlExtensionsPlugin(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            kotlinConfig = kotlinConfig,
            kotlinXmlMappingConfig = kotlinXmlMappingConfig,
            kotlinXmlExtensionsConfig = kotlinXmlExtensionsConfig,
            customExtensionNodes = customExtensionNodes,
            mapperUtil = kotlinXmlMapper
        ).run()

        val rustBase = RustBase(
            protoModelModule = rustMapperConfig.protoModelModule,
            tree = baseTypeNodes,
            protoCustomTypesTree = protoGen.optionalCompanionsRoot,
        )

        val rustGen = RustGenerator(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            config = rustMapperConfig,
            generateCustomTypes = episodeIn == null,
            rustBase = rustBase
        )
        rustGen.run()

        val rustQuickXml = RustQuickXmlParser(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            config = quickXmlParserConfig,
            rustConfig = rustMapperConfig,
            generateCustomTypes = episodeIn == null,
        )
        rustQuickXml.run()

        val rustQuickXmlWriter = RustQuickXmlWriter(
            tree = baseTypeNodes,
            converterConfig = converterConfig,
            rustConfig = rustMapperConfig,
            config = quickXmlWriterConfig,
            generateCustomTypes = episodeIn == null,
        )
        rustQuickXmlWriter.run()


        val plugins: List<BaseNodePlugin> = listOf(
            protoGen,
            kotlinModel,
            kotlinProtoMapper,
            kotlinXmlMapper,
            rustGen,
            rustQuickXml,
            rustQuickXmlWriter,
        )

        converterConfig.episodeLocationOut?.let {
            val episodes = generateEpisode(baseTypeNodes, plugins)
            val string = Json.encodeToString(episodes)

            File(it).writeText(string)

            logger.info("Wrote ${episodes.size} episode entries")
        }
    }

    /**
     * Checks whether an episode list contains the given QName.
     */
    private fun qNameInEpisode(episodeIn: List<PrebuiltIdentifier>?, qName: QName): Boolean {
        if (episodeIn == null) {
            return false
        }
        return episodeIn.filterIsInstance<PrebuiltIdentifier.QNameIdentified>().any { it.qname.toQName() == qName }
    }

    private fun processEpisodeIn(episodeIn: List<PrebuiltIdentifier>?, baseTypeNodes: BaseNode) {
        episodeIn?.let { episodes ->
            episodes.forEach { episode ->
                when (episode) {
                    is PrebuiltIdentifier.QNameIdentified -> {
                        val matchingNodes = baseTypeNodes.children.mapNotNull { child ->
                            when (val nodeType = child.nodeType) {
                                is NodeType.Message -> nodeType
                                else -> null
                            }
                        }.filter { message ->
                            message.qname?.let { qname -> qname.namespaceURI == episode.qname.namespaceUri && qname.localPart == episode.qname.localName }
                                ?: false
                        }
                            .toList()

                        if (matchingNodes.size == 1) {
                            matchingNodes.first().skipGenerating = true
                            matchingNodes.first().episodePluginData = episode.pluginData
                        } else {
                            logger.warn("Could not find exactly node to mark for qname ${episode.qname}, found ${matchingNodes.size}")
                        }
                    }

                    is PrebuiltIdentifier.OneOfIdentified -> {
                        val matchingNodes = baseTypeNodes.children
                            .filter { isOneOfContainer(it) }
                            .map { Pair(it, processOneOneOfToEpisode(it, emptyList())) }
                            .filter { (_, ep) -> ep == episode }
                            .toList()

                        if (matchingNodes.size == 1) {
                            (matchingNodes.first().first.nodeType as NodeType.Message).skipGenerating = true
                            (matchingNodes.first().first.nodeType as NodeType.Message).episodePluginData =
                                episode.pluginData

                        } else {
                            logger.warn("Could not find exactly node to mark for OneOf $episode, found ${matchingNodes.size}")
                        }
                    }
                }
            }
        }
    }

    /**
     * Qualified name which can be serialized.
     *
     * [javax.xml.namespace.QName] can't be serialized, and also allows prefix, which we do not care about.
     */
    @Serializable
    data class SerializableQName(val namespaceUri: String, val localName: String) {
        companion object {
            fun fromQName(qname: QName): SerializableQName {
                return SerializableQName(qname.namespaceURI, qname.localPart)
            }
        }

        fun toQName(): QName {
            return QName(namespaceUri, localName)
        }
    }


    @Serializable
    sealed class PrebuiltIdentifier(val pluginData: Map<String, EpisodeData>) {

        @Serializable
        data class QNameIdentified(private val _pluginData: Map<String, EpisodeData>, val qname: SerializableQName) :
            PrebuiltIdentifier(_pluginData) {
            override fun equals(other: Any?) = other is QNameIdentified && this.qname == other.qname
            override fun hashCode(): Int {
                return qname.hashCode()
            }
        }

        @Serializable
        data class OneOfIdentified(
            private val _pluginData: Map<String, EpisodeData>,
            val name: String,
            val parameters: List<Pair<String, SerializableQName>>
        ) : PrebuiltIdentifier(_pluginData) {
            override fun equals(other: Any?) =
                other is OneOfIdentified && this.name == other.name && this.parameters == other.parameters

            override fun hashCode(): Int {
                var result = name.hashCode()
                result = 31 * result + parameters.hashCode()
                return result
            }
        }

    }

    private fun generateEpisode(tree: BaseNode, plugins: List<BaseNodePlugin>): List<PrebuiltIdentifier> {
        check(tree.nodeType is NodeType.Root)

        val identifiers: List<PrebuiltIdentifier> = tree.children.mapNotNull {
            when (val nodeType = it.nodeType) {
                is NodeType.Message -> {
                    when (isOneOfContainer(it)) {
                        false -> {
                            checkNotNull(nodeType.qname) {
                                "$it had no QName"
                            }

                            val pluginData = plugins.associate { plugin ->
                                plugin.pluginIdentifier() to plugin.episodeData(it)
                            }

                            PrebuiltIdentifier.QNameIdentified(
                                pluginData,
                                qname = SerializableQName.fromQName(nodeType.qname!!)
                            )
                        }

                        true -> {
                            processOneOneOfToEpisode(it, plugins)
                        }
                    }
                }

                else -> {
                    logger.debug { "Do not care about ${it.nodeType}" }
                    null
                }
            }
        }

        return identifiers
    }
}