package org.somda.protosdc_converter.converter.kotlinmapper.proto

import org.somda.protosdc_converter.converter.Proto3Generator
import org.somda.protosdc_converter.converter.kotlin.KotlinEntry
import org.somda.protosdc_converter.converter.kotlinmapper.MapperMetadataRegistry
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.EpisodeData
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

const val KOTLIN_PROTO_MAPPER_PLUGIN_IDENTIFIER_V2 = "kotlinprotomapperv2"
const val KOTLIN_PROTO_MAPPER_TOML_TABLE_NAME = "KotlinProtoMapping"

/**
 * Gets the episode data for the Kotlin Mapper plugin.
 */
fun NodeType.Message.kotlinProtoMapperEpisodeMapperData(): EpisodeData.Mapper? {
    return episodePluginData[KOTLIN_PROTO_MAPPER_PLUGIN_IDENTIFIER_V2] as? EpisodeData.Mapper
}

/**
 * Marks a cluster as handled for the KotlinToProto mapper.
 */
fun BaseNode.kotlinToProtoSetClusterHandled() {
    clusterHandled[OutputLanguage.KotlinToProtoMapper] = true
}

/**
 * Checks if a cluster was handled already for the KotlinToProto mapper.
 */
fun BaseNode.kotlinToProtoClusterHandled() = clusterHandled[OutputLanguage.KotlinToProtoMapper] ?: false

/**
 * Marks a cluster as handled for the ProtoToKotlin mapper.
 */
fun BaseNode.protoToKotlinSetClusterHandled() {
    clusterHandled[OutputLanguage.ProtoToKotlinMapper] = true
}

/**
 * Checks if a cluster was handled already for the ProtoToKotlin mapper.
 */
fun BaseNode.protoToKotlinClusterHandled() = clusterHandled[OutputLanguage.ProtoToKotlinMapper] ?: false

/**
 * Interface to express functionality relevant to both mapping directions.
 */
interface KotlinProtoMapperUtil {
    fun fullyQualifiedProtoNameFor(baseNode: BaseNode): String
    fun fullyQualifiedKotlinNameFor(baseNode: BaseNode): String
    fun protoToKotlinType(proto: String): String
    fun getProtoFieldName(proto: Proto3Generator.ProtoEntry, pascalCase: Boolean = false): String
    fun getKotlinFieldName(kotlin: KotlinEntry): String
    fun toProtoEnumName(value: String): String

    val protoToKotlinMapperMap: MapperMetadataRegistry

    val kotlinToProtoMapperMap: MapperMetadataRegistry
}