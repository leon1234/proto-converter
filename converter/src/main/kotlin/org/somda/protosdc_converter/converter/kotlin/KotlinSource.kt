package org.somda.protosdc_converter.converter.kotlin

import java.util.*

data class CodeLine(val indentOffset: Int, val line: String) {
    constructor(line: String) : this(0, line)
}

/**
 * Utility functions to create various Kotlin code particles.
 *
 * @param indentationSize number of blanks for a single indentation.
 * @param oneOfClassPrefix a prefix for data classes that represent a OneOf item, needed to create a separate type name
 *                         different from the enclosed data type of the OneOf item.
 * @param oneOfClassMemberName the type enclosed by a OneOf item gets this as a property name.
 */
class KotlinSource(
    indentationSize: Int,
    private val oneOfClassPrefix: String,
    private val oneOfClassMemberName: String
) {
    private val singleIndentation = " ".repeat(indentationSize)

    fun packageDeclaration(packagePath: String) = "package $packagePath"

    fun importDeclaration(importPath: String) = "import $importPath"

    fun dataClassPrimaryConstructorStart(className: String) = "data class $className("

    fun blockStart() = " {"

    fun list(listType: String) = "List<$listType>"
    fun defaultedListParameter(listType: String) = assignment(
        lhs = list(listType),
        rhs = "listOf()"
    )

    fun defaultedNullableDataClassParameter(
        parameterName: String,
        parameterType: String,
        nullable: Boolean
    ): String {
        val nullableDecl = if (nullable) {
            "? = null"
        } else {
            ""
        }
        return "val $parameterName: $parameterType$nullableDecl,"
    }

    fun enumStart(className: String) = "enum class $className {"
    fun enumItem(name: String) = "$name,"

    fun oneOfStart(className: String) = "sealed class $className {"
    fun oneOfEntry(
        parameterType: String,
        sealedBaseType: String
    ): String {
        return "data class $oneOfClassPrefix$parameterType(val $oneOfClassMemberName: $parameterType) : $sealedBaseType()"
    }

    fun objectStart(classNamePart: String) ="object $classNamePart {"

    fun whenStart(whenSubject: String) = "when ($whenSubject) {"

    fun whenEntry(condition: String, statement: String) = "$condition -> $statement"

    fun ifStart(condition: String) = "if ($condition) {"

    fun returnAny(statement: String) = "return $statement"

    fun assignment(lhs: String, rhs: String) = "$lhs = $rhs"

    fun valAssignment(varName: String, rhs: String) = assignment("val $varName", rhs)

    fun blockEnd() = "}"

    fun constructorEnd() = ")"

    fun indentLines(baseLevel: Int = 0, lines: () -> List<CodeLine>): String =
        lines().joinToString("\n") { indent(baseLevel + it.indentOffset) { it.line } }

    fun indent(level: Int, line: () -> String) = line().split("\n").joinToString("\n") {
        if (it.trim().isNotEmpty()) {
            singleIndentation.repeat(level) + it.trimEnd()
        } else {
            it.trimEnd()
        }
    }

    companion object {
        /**
         * Converts the given text to a parameter name, turning the first character into lowercase.
         *
         * @param text the identifier to create a parameter name from, typically an XML Schema name.
         */
        fun toParameterName(text: String) = text.replaceFirstChar { it.lowercase(Locale.getDefault()) }

        /**
         * Joins the passed package particles to a combined package path.
         *
         * Particles are string-concatenated by using the Kotlin package delimiter.
         */
        fun qualifiedPathFrom(vararg particles: String) = particles.joinToString(".")
    }
}