package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlWriterConfig
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.SELF
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.simpleWriterSignature
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class StringEnumWriter(val writerName: String, val dataType: BaseNode) : WriterTracker {
    override fun generateWriter(
        config: QuickXmlWriterConfig,
        primitiveWriters: MutableMap<BaseNode, RustWriter>,
        simpleWriters: MutableMap<BaseNode, RustWriter>,
        complexWriters: MutableMap<BaseNode, RustWriter>
    ): String {
        check(dataType.children.size == 2)

        val dataTypeRustStruct = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

        val stringEnum = dataType.children[0]
        val nodeTypeEnumeration = stringEnum.nodeType!! as NodeType.StringEnumeration
        val rustStringEnum = stringEnum.languageType[OutputLanguage.Rust]!! as RustEntry.RustStringEnumeration

        val fullyQualifiedRustTypeName = dataTypeRustStruct.fullyQualifiedName(config.modelRustModuleSubstitute)
        val fullyQualifiedRustEnumTypeName = rustStringEnum.fullyQualifiedName(config.modelRustModuleSubstitute)

        val start = """${simpleWriterSignature(fullyQualifiedRustTypeName)}
${INDENT.repeat(2)}Ok(match $SELF.enum_type {
"""

        val payload =
            nodeTypeEnumeration.values.zip(rustStringEnum.enumValues)
                .joinToString(separator = "") { (xml, rust) ->
                    "${INDENT.repeat(3)}$fullyQualifiedRustEnumTypeName::$rust => b\"$xml\".to_vec(),\n"
                }

        val end = """${INDENT.repeat(2)}})
    }
}
            """

        return start + payload + end
    }
}