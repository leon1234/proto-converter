package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.somda.protosdc.mapping.base.KotlinToXmlBaseTypes
import org.somda.protosdc.mapping.base.XmlToKotlinBaseTypes
import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlinmapper.FullMappingDefinition
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.xmlprocessor.Constants


/**
 * Interface for custom Kotlin <-> XML mappings.
 */
interface XmlMapping {
    /**
     * Returns mapper metadata from Proto to Kotlin.
     */
    fun fromXml(): FullMappingDefinition

    /**
     * Returns mapper metadata from Kotlin to Proto.
     */
    fun toXml(): FullMappingDefinition
}

/**
 * Predefined custom mapping of extensions.
 */
class ExtensionItemMapping(kotlinPackage: String) : XmlMapping {
    private val kotlinQualifiedName = KotlinSource.qualifiedPathFrom(kotlinPackage, "Extension")

    override fun fromXml() = FullMappingDefinition(
        Constants.extQName("Extension").toString(),
        kotlinQualifiedName,
        FROM_XML_FUNCTION_NAME,
        listOf(
            CodeLine("fun $FROM_XML_FUNCTION_NAME(value: ${KotlinType.DOM_NODE}): $kotlinQualifiedName {"),
            CodeLine(1, "return $kotlinQualifiedName(${NodeListToList.FUNCTION_NAME}(value.childNodes)"),
            CodeLine(2, ".filter { it.nodeType == ${KotlinType.DOM_NODE}.ELEMENT_NODE }"),
            CodeLine(2, ".map {"),
            CodeLine(
                3,
                """val mustUnderstand = when (val attr = it.attributes.getNamedItemNS("${Constants.EXT_NAMESPACE}", "MustUnderstand")) {"""
            ),
            CodeLine(4, "null -> false"),
            CodeLine(4, "else -> $mapBoolFromXml(attr)"),
            CodeLine(3, "}"),
            CodeLine(3, "$kotlinQualifiedName.Item(mustUnderstand, $mapAnyFromXml(it))"),
            CodeLine(2, "}"),
            CodeLine(1, ")"),
            CodeLine("}")
        )
    )

    override fun toXml() = FullMappingDefinition(
        kotlinQualifiedName,
        Constants.extQName("Extension").toString(),
        TO_XML_FUNCTION_NAME,
        listOf(
            CodeLine("fun $TO_XML_FUNCTION_NAME(value: $kotlinQualifiedName, parent: ${KotlinType.DOM_ELEMENT}) {"),
            CodeLine(1, "for (item in value.item) {"),
            CodeLine(2, "$mapAnyToXml(item, parent)"),
            CodeLine(2, "val generatedElement = try {"),
            CodeLine(3, "parent.childNodes.item(parent.childNodes.length - 1) as org.w3c.dom.Element"),
            CodeLine(2, "} catch(e: Exception) {"),
            CodeLine(3, "error(\"Extension mapping from \${value::class} to XML must not fail. Error message: \${e.message}\")"),
            CodeLine(2, "}"),
            CodeLine(
                2,
                "parent.ownerDocument.createAttributeNS(\"http://standards.ieee.org/downloads/11073/11073-10207-2017/extension\", \"MustUnderstand\").also {"
            ),
            CodeLine(3, "generatedElement.attributes.setNamedItemNS(it)"),
            CodeLine(3, "applyPrefix(it, \"http://standards.ieee.org/downloads/11073/11073-10207-2017/extension\")"),
            CodeLine(3, "org.somda.protosdc.mapping.base.KotlinToXmlBaseTypes.mapBoolean(item.mustUnderstand, it)"),
            CodeLine(2, "}"),
            CodeLine(1, "}"),
            CodeLine("}")
        )
    )

    private companion object {
        const val FROM_XML_FUNCTION_NAME = "mapExtension"
        const val TO_XML_FUNCTION_NAME = "mapExtension"

        val mapBoolFromXml = KotlinSource.qualifiedPathFrom(XmlToKotlinBaseTypes::class.qualifiedName!!, "mapBoolean")
        val mapAnyFromXml = KotlinSource.qualifiedPathFrom(XmlToKotlinBaseTypes::class.qualifiedName!!, "mapAny")

        val mapAnyToXml = KotlinSource.qualifiedPathFrom(KotlinToXmlBaseTypes::class.qualifiedName!!, "mapAny")
    }
}

/**
 * Predefined custom mapping of QNames.
 */
class QNameMapping(kotlinPackage: String) : XmlMapping {
    private val kotlinQualifiedName = KotlinSource.qualifiedPathFrom(kotlinPackage, "QualifiedName")

    override fun fromXml(): FullMappingDefinition {
        return FullMappingDefinition(
            Constants.xsdQName("QName").toString(),
            kotlinQualifiedName,
            FROM_XML_FUNCTION_NAME,
            listOf(
                CodeLine("fun $FROM_XML_FUNCTION_NAME(value: ${KotlinType.DOM_NODE}): $kotlinQualifiedName {"),
                CodeLine(1, "val qName = ${XmlToKotlinBaseTypes.javaClass.canonicalName}.mapQName(value)"),
                CodeLine(1, "return $kotlinQualifiedName(qName.namespaceURI, qName.localPart)"),
                CodeLine("}")
            )
        )
    }

    override fun toXml() = FullMappingDefinition(
        kotlinQualifiedName,
        Constants.xsdQName("QName").toString(),
        TO_XML_FUNCTION_NAME,
        listOf(
            CodeLine("fun $TO_XML_FUNCTION_NAME(value: $kotlinQualifiedName, node: ${KotlinType.DOM_NODE}): ${KotlinType.DOM_NODE} {"),
            CodeLine(
                1,
                "return ${KotlinToXmlBaseTypes.javaClass.canonicalName}.mapQName(${KotlinType.QNAME}(value.namespace, value.localName), node)"
            ),
            CodeLine("}")
        )
    )

    private companion object {
        const val FROM_XML_FUNCTION_NAME = "mapXmlQNameToQualifiedName"
        const val TO_XML_FUNCTION_NAME = "mapQualifiedNameToXmlQName"
    }
}