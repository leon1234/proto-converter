package org.somda.protosdc_converter.converter.kotlinmapper.xml

@kotlinx.serialization.Serializable
data class KotlinXmlExtensionsConfig(
    val mappingPackage: String,
    val outputFolders: List<String> = emptyList(),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
    val outputClassName: String = "XmlExtensionsMapper",
    val fullyQualifiedInterfaceName: String? = null,
)