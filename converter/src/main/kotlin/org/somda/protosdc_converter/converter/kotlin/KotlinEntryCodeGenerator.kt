package org.somda.protosdc_converter.converter.kotlin

import org.somda.protosdc_converter.converter.kotlin.entrygen.*
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import java.io.PrintWriter

/**
 * Core class that generates actual .kt file content.
 */
class KotlinEntryCodeGenerator(private val kotlinSource: KotlinSource) {
    /**
     * Creates code based on the given node.
     *
     * @param node to generate Kotlin code for.
     * @param target the target in which to write the proto content.
     * @param level the level of the current node, used for indentation.
     * @param isNestedCall when the call is for a nested body, i.e. the body of a data class.
     */
    fun generateDataClassCode(node: BaseNode, target: PrintWriter, level: Int = 0, isNestedCall: Boolean = false) {
        if (node.ignore) {
            return
        }

        val kotlinEntry = node.kotlinLanguageType()

        val skipNested = when (kotlinEntry) {
            is KotlinEntry.DataClass -> kotlinEntry.onlyGenerateNested
            is KotlinEntry.KotlinOneOf -> kotlinEntry.onlyGenerateNested
            is KotlinEntry.KotlinStringEnumeration -> kotlinEntry.onlyGenerateNested
            else -> false
        } && !isNestedCall

        if (skipNested) {
            return
        }

        val skipType = when (kotlinEntry) {
            is KotlinEntry.DataClass -> kotlinEntry.skipType
            else -> false
        }

        if (skipType) {
            node.children.forEach {
                generateDataClassCode(it, target, level)
            }
            return
        }

        val mainOffset = enter(kotlinEntry, level).also {
            if (it.isNotBlank()) {
                // only write non-empty lines
                target.println(it)
            }
        }.let {
            // only apply a + 1 offset when we didn't generate a header, i.e. this is just a parameter or something
            if (it.isBlank()) {
                level
            } else {
                level + 1
            }
        }

        node.children.forEach {
            generateDataClassCode(it, target, mainOffset)
        }

        main(kotlinEntry, mainOffset).also {
            if (it.isNotBlank()) {
                target.println(it)
            }
        }

        exit(kotlinEntry, level).also {
            if (it.isNotEmpty()) {
                target.println(it)
            }
        }
    }

    private fun enter(entry: KotlinEntry, level: Int): String {
        return when (entry) {
            is KotlinEntry.DataClass -> DataClassGen(kotlinSource, entry, level) { node, nestedLevel, target ->
                generateDataClassCode(node, target, nestedLevel, true)
            }.enter()

            is KotlinEntry.KotlinOneOf -> OneOfGen(kotlinSource, entry, level).enter()
            is KotlinEntry.KotlinOneOfParameter -> OneOfParameterGen(kotlinSource, entry, level).enter()
            is KotlinEntry.KotlinParameter -> ParameterGen(kotlinSource, entry, level).enter()
            is KotlinEntry.KotlinStringEnumeration -> StringEnumerationGen(kotlinSource, entry, level).enter()
        }
    }

    private fun exit(entry: KotlinEntry, level: Int): String {
        return when (entry) {
            is KotlinEntry.DataClass -> DataClassGen(kotlinSource, entry, level) { node, nestedLevel, target ->
                generateDataClassCode(node, target, nestedLevel, true)
            }.exit()

            is KotlinEntry.KotlinOneOf -> OneOfGen(kotlinSource, entry, level).exit()
            is KotlinEntry.KotlinOneOfParameter -> OneOfParameterGen(kotlinSource, entry, level).exit()
            is KotlinEntry.KotlinParameter -> ParameterGen(kotlinSource, entry, level).exit()
            is KotlinEntry.KotlinStringEnumeration -> StringEnumerationGen(kotlinSource, entry, level).exit()
        }
    }

    private fun main(entry: KotlinEntry, level: Int): String {
        return when (entry) {
            is KotlinEntry.DataClass -> DataClassGen(kotlinSource, entry, level) { node, nestedLevel, target ->
                generateDataClassCode(node, target, nestedLevel, true)
            }.main()

            is KotlinEntry.KotlinOneOf -> OneOfGen(kotlinSource, entry, level).main()
            is KotlinEntry.KotlinOneOfParameter -> OneOfParameterGen(kotlinSource, entry, level).main()
            is KotlinEntry.KotlinParameter -> ParameterGen(kotlinSource, entry, level).main()
            is KotlinEntry.KotlinStringEnumeration -> StringEnumerationGen(kotlinSource, entry, level).main()
        }
    }
}