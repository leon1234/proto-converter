package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.ROOT_STATE
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.toEnd
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.toStart
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import javax.xml.namespace.QName

// groups children by type of collection according to XML Schema.
sealed interface ChildCollection {
    val elements: List<ParserElementInfo>
    // sequence is the only supported collection type right now, the others are just for completionists...
    data class Sequence(override val elements: List<ParserElementInfo>) : ChildCollection
    data class All(override val elements: List<ParserElementInfo>) : ChildCollection
    data class Choice(override val elements: List<ParserElementInfo>) : ChildCollection
    data class Failed(override val elements: List<ParserElementInfo>) : ChildCollection
}

interface ElementInfo {
    val elementStepName: String
    val elementQName: QName
    val elementTypeQName: QName?
    val elementNode: BaseNode
    val elementType: BaseNode?
    val contentType: BaseNode?
    val contentAccess: String?
    // or has children, never both, because mixed mode can suck a diesel exhaust pipe
    val ancestorTypes: List<BaseNode>

    val isOptional: Boolean
    val isList: Boolean

}

data class WriterElementInfo(
    override val elementStepName: String,
    override val elementQName: QName,
    override val elementTypeQName: QName?,
    val attributes: List<WriterAttributeInfo>,
    override val elementNode: BaseNode,
    override val elementType: BaseNode?,
    val elementTypeWriter: RustWriter?,
    // either has content
    val contentWriterPrimitive: RustWriter? = null,
    val contentWriterSimple: RustWriter? = null,
    override val contentType: BaseNode? = null,
    override val contentAccess: String? = null,
    // or has children, never both, because mixed mode can suck a diesel exhaust pipe
    override val ancestorTypes: List<BaseNode> = emptyList(),

    override val isOptional: Boolean,
    override val isList: Boolean,
): ElementInfo

data class ParserElementInfo(
    override val elementStepName: String,
    override val elementQName: QName,
    override val elementTypeQName: QName?,
    val attributes: List<ParserAttributeInfo>,
    override val elementNode: BaseNode,
    override val elementType: BaseNode?,
    val elementTypeParser: RustParser?,
    // either has content
    val contentParserPrimitive: RustParser? = null,
    val contentParserSimple: RustParser? = null,
    override val contentType: BaseNode? = null,
    override val contentAccess: String? = null,
    // or has children, never both, because mixed mode can suck a diesel exhaust pipe
    val children: List<ChildCollection>? = emptyList(),
    override val ancestorTypes: List<BaseNode> = emptyList(),

    override val isOptional: Boolean,
    override val isList: Boolean,
): ElementInfo {

    fun contentTypeName(config: QuickXmlParserConfig, dataType: BaseNode?): String {
        this.contentType!!.let {
            return when (val langType = it.languageType[OutputLanguage.Rust]) {
                is RustEntry.RustStruct -> {
                    langType.fullyQualifiedName(config.modelRustModuleSubstitute)
                }

                is RustEntry.RustOneOf -> {
                    // get parent module path
                    val parentMessage = dataType!!.languageType[OutputLanguage.Rust] as RustEntry.RustStruct
                    parentMessage.fullyQualifiedName(config.modelRustModuleSubstitute)
                }

                else -> TODO("Unhandled node type $langType")
            }
        }
    }

    fun getStates(): List<String> {
        val relevantElements = mutableListOf(
            ROOT_STATE, // root is always the first state
            toStart(elementStepName),
            toEnd(elementStepName)
        )

        val states = children?.flatMap {
            it.elements.filter { elem -> !isAncestorType(elem) }
                .map { elem -> elem.elementStepName }
        }?.flatMap { element -> listOf(toStart(element), toEnd(element)).asIterable() }?.toList()
            ?: emptyList()

        return relevantElements + states
    }

    internal data class ChildIndex(val containerIndex: Int, val childInContainerIndex: Int, val container: ChildCollection)

    private fun findChildIndex(child: ParserElementInfo): ChildIndex {
        checkNotNull(children) { "Element $this does not contain children" }

        val childContainer = checkNotNull(children.find { it.elements.contains(child) }) { "Child is not known" }
        val childContainerIndex = children.indexOf(childContainer)
        check(childContainerIndex >= 0) { "Child container not known" }
        val childInContainerIndex = childContainer.elements.indexOf(child)
        check(childInContainerIndex >= 0) { "Child not known in container" }

        return ChildIndex(childContainerIndex, childInContainerIndex, childContainer)
    }

    private fun findMandatoryPredecessor(
        children: List<ChildCollection>,
        child: ParserElementInfo
    ): ParserElementInfo? {
        // flatten all children, filter ancestor types
        val listOfChildren = children
            .flatMap { it.elements }
            .filter { !isAncestorType(it) }
            .toList()

        // find position of child
        val childPosition = listOfChildren.indexOf(child)
        check(childPosition >= 0) { "Child ${child.elementNode.nodeName} of type ${child.elementQName} not known" }

        val predecessors = listOfChildren.subList(0, childPosition)
        val mandatoryPredecessorIndex: Int = predecessors.reversed().lastOrNull { !it.isOptional }?.let {
            predecessors.indexOf(it)
        } ?: -1

        return when (mandatoryPredecessorIndex) {
            -1 -> null
            else -> predecessors[mandatoryPredecessorIndex]
        }
    }

    // end is not included
    private fun getInbetweenElements(
        children: List<ChildCollection>,
        start: ParserElementInfo,
        end: ParserElementInfo
    ): List<ParserElementInfo> {
        if (start == end) {
            return emptyList()
        }

        val (startContainerIndex, startChildIndex, startChildContainer) = findChildIndex(start)
        val (endContainerIndex, endChildIndex, endChildContainer) = findChildIndex(end)

        val containersToCheck = children.subList(startContainerIndex, endContainerIndex + 1)

        val startElements: List<ParserElementInfo> = when (startChildContainer) {
            // only from start element to end
            is ChildCollection.Sequence -> startChildContainer.elements.subList(
                startChildIndex,
                startChildContainer.elements.size
            )
            // all elements in choice allowed
            is ChildCollection.Choice -> startChildContainer.elements
            else -> TODO("Unsupported container type ${startChildContainer.javaClass.simpleName}")
        }

        val endElements: List<ParserElementInfo> = when (endChildContainer) {
            // only from start of list to element (exclusive)
            is ChildCollection.Sequence -> endChildContainer.elements.subList(0, endChildIndex)
            // no other elements from choice allowed
            is ChildCollection.Choice -> emptyList()
            else -> TODO("Unsupported container type ${startChildContainer.javaClass.simpleName}")
        }

        val inBetweenElements: List<ParserElementInfo> = containersToCheck
            .filter { it != startChildContainer && it != endChildContainer }
            .map {
                when (it) {
                    is ChildCollection.Sequence -> it.elements
                    is ChildCollection.Choice -> it.elements
                    else -> TODO("Unsupported container type ${startChildContainer.javaClass.simpleName}")
                }
            }
            .flatten()

        // remove any ancestor type
        return startElements.filter { !isAncestorType(it) } +
            inBetweenElements.filter { !isAncestorType(it) } +
            endElements.filter { !isAncestorType(it) }
    }

    fun getValidTransitionsForChild(child: ParserElementInfo): List<String> {
        checkNotNull(children) { "Element $this does not contain children" }
        val children = children

        // determine position of child in collections
        val (_, _, childContainer) = findChildIndex(child)

        // check for first mandatory predecessor in container
        val mandatoryPredecessor = when (childContainer) {
            is ChildCollection.Choice -> {
                // if this is a choice, we don't need to look into this container,
                // allowed predecessors are before *the first* element of our choice
                findMandatoryPredecessor(children, childContainer.elements.first())
            }
            is ChildCollection.Sequence -> {
                findMandatoryPredecessor(children, child)
            }
            else -> TODO("ChildCollection ${childContainer.javaClass.simpleName} not supported")
        }

        // determine allowed predecessors:
        val mandatoryPredecessorContainer = mandatoryPredecessor?.let { findChildIndex(it).container }
        val allowedPredecessors: List<ParserElementInfo> = mandatoryPredecessorContainer?.let {
            when (it) {
                is ChildCollection.Choice -> {
                    // everything in this choice and everything in between
                    getInbetweenElements(children, it.elements.first(), child)
                }
                is ChildCollection.Sequence -> {
                    // mandatory predecessor and everything between it and the child
                    getInbetweenElements(children, mandatoryPredecessor, child)
                }
                else -> TODO("ChildCollection ${childContainer.javaClass.simpleName} not supported")
            }
        } ?: run {
            // everything before this is a possible predecessor
            when (childContainer) {
                is ChildCollection.Choice -> getInbetweenElements(children, children.first().elements.first(), childContainer.elements.first())
                is ChildCollection.Sequence -> getInbetweenElements(children, children.first().elements.first(), child)
                else -> TODO("ChildCollection ${childContainer.javaClass.simpleName} not supported")
            }

        }

        // if this step is repeatable, it itself is an allowed predecessor
        val fullPredecessors = when (child.isList) {
            false -> allowedPredecessors
            true -> allowedPredecessors + listOf(child)
        }

        val actualStepNames = fullPredecessors.map { toEnd(it.elementStepName) }.toList()

        return when (mandatoryPredecessorContainer) {
            // if this has no mandatory predecessors, the "root" element i.e. the start of the first element is fine as well
            null -> {
                val firstElem = getStates()[1]
                listOf(firstElem) + actualStepNames
            }
            else -> actualStepNames
        }
    }

    fun getFilteredChildren(): Sequence<ParserElementInfo> {
        return children?.flatMap { collection ->
            collection.elements.filter { !isAncestorType(it) }
        }?.asSequence() ?: emptySequence()
    }

    /**
     * Determines if this element represents an ancestor type,
     * i.e. a parameter to reference something this type inherited from
     */
    fun isAncestorType(element: ParserElementInfo): Boolean {
        return when (val itType = element.elementNode.nodeType) {
            is NodeType.Parameter -> {
                // filter out the inherited types
                ancestorTypes.contains(itType.parameterType.parent)
            }
            // filter out child elements which are builtin types like string
            else -> ancestorTypes.contains(element.elementNode)
        }
    }
}
