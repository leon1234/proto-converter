package org.somda.protosdc_converter.converter

@kotlinx.serialization.Serializable
data class ProtoConfig(
    val protoPackage: String,
    val importPrefix: String = "",
    val messageNameSuffix: String,
    val outerJavaClassNameSuffix: String = "Proto",
    val optionJavaMultipleFiles: Boolean = false,
    val optionJavaPackage: String = "",
    val outputFolders: List<String> = listOf("proto/"),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
) {
    init {
        require(importPrefix.isBlank() || importPrefix.endsWith("/"))
        outputFolders.forEach { require(it.endsWith("/")) }
        outputFoldersNoHierarchy.forEach { require(it.endsWith("/")) }
    }
}