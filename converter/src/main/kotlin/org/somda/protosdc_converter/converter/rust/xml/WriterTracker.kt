package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlWriterConfig
import org.somda.protosdc_converter.xmlprocessor.BaseNode


interface WriterTracker {
    fun generateWriter(
        config: QuickXmlWriterConfig,
        primitiveWriters: MutableMap<BaseNode, RustWriter>,
        simpleWriters: MutableMap<BaseNode, RustWriter>,
        complexWriters: MutableMap<BaseNode, RustWriter>
    ): String
}