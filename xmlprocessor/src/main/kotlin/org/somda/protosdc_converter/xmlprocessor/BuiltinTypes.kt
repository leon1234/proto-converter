package org.somda.protosdc_converter.xmlprocessor

import javax.xml.namespace.QName

enum class BuiltinTypes(val qname: QName) {
    XSD_ANY(Constants.xsdQName("any")),
    XSD_ANY_SIMPLE_TYPE(Constants.xsdQName("anySimpleType")),
    XSD_ANY_URI(Constants.xsdQName("anyURI")),
    XSD_BOOL(Constants.xsdQName("boolean")),
    XSD_DATE(Constants.xsdQName("date")),
    XSD_DATE_TIME(Constants.xsdQName("dateTime")),
    XSD_DECIMAL(Constants.xsdQName("decimal")),
    XSD_DURATION(Constants.xsdQName("duration")),
    XSD_G_YEAR(Constants.xsdQName("gYear")),
    XSD_G_YEAR_MONTH(Constants.xsdQName("gYearMonth")),
    XSD_INT(Constants.xsdQName("int")),
    XSD_INTEGER(Constants.xsdQName("integer")),
    XSD_LANGUAGE(Constants.xsdQName("language")),
    XSD_LONG(Constants.xsdQName("long")),
    XSD_QNAME(Constants.xsdQName("QName")),
    XSD_STRING(Constants.xsdQName("string")),
    XSD_UINT(Constants.xsdQName("unsignedInt")),
    XSD_ULONG(Constants.xsdQName("unsignedLong"));

    companion object {
        fun isBuiltinType(qname: QName): Boolean {
            values().forEach {
                if (it.qname == qname) {
                    return true
                }
            }
            return false
        }

        fun getBuiltinType(qname: QName): BuiltinTypes? {
            values().forEach {
                if (it.qname == qname) {
                    return it
                }
            }
            return null
        }
    }
}