package org.somda.protosdc_converter.xmlprocessor

/**
 * Wrapper class to differentiate the return values of generators such as [BaseTypeProcessor.generateElement],
 * which can be both simple parameters and actual messages.
 */
sealed class ReturnType {

    /**
     * Generator created a new message
     */
    data class Message(val node: BaseNode) : ReturnType()

    /**
     * Generator created multiple parameters. These can also contain messages, but in that case the parameter
     * referencing the new message is part of the list.
     */
    data class Parameters(val nodes: List<BaseNode>) : ReturnType()

    /**
     * Extension containing the baseNode which was extended, as well as
     * all new parameters which are in the new extension.
     */
    data class Extension(val parameters: Parameters, val baseNode: BaseNode) : ReturnType()

}