package org.somda.protosdc_converter.xmlprocessor

@kotlinx.serialization.Serializable
sealed class EpisodeData {

    @kotlinx.serialization.Serializable
    data class Model(
        val packageName: String,
        val typeName: String
    ) : EpisodeData()

    @kotlinx.serialization.Serializable
    data class Mapper(
        val packageName: String,
        val sourceToTargetFunction: String,
        val targetToSourceFunction: String,
        val sourceStructName: String,
        val sourceTypeName: String,
        val targetStructName: String,
        val targetTypeName: String
    ) : EpisodeData()
}