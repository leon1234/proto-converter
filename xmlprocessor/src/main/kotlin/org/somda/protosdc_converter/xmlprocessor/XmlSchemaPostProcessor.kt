package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.apache.ws.commons.schema.*
import org.apache.ws.commons.schema.utils.XmlSchemaNamed
import org.apache.ws.commons.schema.utils.XmlSchemaObjectBase
import java.io.File
import java.io.FileInputStream
import javax.xml.namespace.QName
import javax.xml.transform.stream.StreamSource

/**
 * Reads in an XML Schema including other imported XML Schemas and creates a dependency tree.
 *
 * The dependency tree is a tree of [XmlSchemaNode] objects with all types listed as children to the root level
 * ordered in a way that a type in the list at position N only relies on types with a list index lower than N.
 *
 * @param topLevelFile File path of the top level XML Schema.
 * @param loopThreshold The algorithm to resolve dependencies is brute force. In each iteration all types are added to
 *                      the tree for which in a previous loop all dependent types could be resolved, starting with
 *                      builtin types string, int, URI, etc. If loopThreshold is too low, it can happen that in the
 *                      end there are missing types in the tree.
 */
class XmlSchemaPostProcessor(
    topLevelFile: File,
    private val loopThreshold: Int
) {
    private val schemaCollection: XmlSchemaCollection = XmlSchemaCollection()

    private var dependencyTree = XmlSchemaNode(null, null)

    init {
        logger.info { "Reading XML Schema(s) from '${topLevelFile.canonicalPath}'" }
        schemaCollection.setBaseUri(topLevelFile.absolutePath)
        schemaCollection.read(StreamSource(FileInputStream(topLevelFile)))
        cleanSlate()
    }

    /**
     * Starts resolving.
     *
     * @return The resolved dependency tree.
     */
    fun resolve(): XmlSchemaNode {
        logger.info("Start resolving the tree...")
        cleanSlate()
        resolveAllOf(
            unresolvedSimpleTypes = createUnresolved(),
            unresolvedComplexTypes = createUnresolved(),
            unresolvedElements = createUnresolved(),
            unresolvedAttributeGroups = createUnresolved(),
            unresolvedAttributes = createUnresolved()
        )

        return dependencyTree.clone()
    }

    private fun cleanSlate() {
        dependencyTree = XmlSchemaNode(null, null)
        BuiltinTypes.values().forEach {
            logger.info { "Append builtin type to tree: ${it.qname}" }
            val node = XmlSchemaNode(it.qname, null)
            dependencyTree.addChild(node)
        }
    }

    private fun resolveAllOf(
        unresolvedSimpleTypes: MutableMap<QName, XmlSchemaSimpleType>,
        unresolvedComplexTypes: MutableMap<QName, XmlSchemaComplexType>,
        unresolvedElements: MutableMap<QName, XmlSchemaElement>,
        unresolvedAttributeGroups: MutableMap<QName, XmlSchemaAttributeGroup>,
        unresolvedAttributes: MutableMap<QName, XmlSchemaAttribute>
    ) {
        var count = 0
        while ((unresolvedSimpleTypes.isNotEmpty()
                    || unresolvedComplexTypes.isNotEmpty()
                    || unresolvedElements.isNotEmpty()
                    || unresolvedAttributeGroups.isNotEmpty()
                    || unresolvedAttributes.isNotEmpty())
            && count++ < loopThreshold
        ) {
            logger.info { "Loop round $count" }

            singleResolveLoop(unresolvedSimpleTypes) { type, treeCopy -> resolveSimpleType(type, treeCopy) }
            singleResolveLoop(unresolvedComplexTypes) { type, treeCopy -> resolveComplexType(type, treeCopy) }
            singleResolveLoop(unresolvedElements) { type, treeCopy -> resolveElement(type, treeCopy) }
            singleResolveLoop(unresolvedAttributes) { type, treeCopy -> resolveAttribute(type, treeCopy) }
            singleResolveLoop(unresolvedAttributeGroups) { type, treeCopy -> resolveAttributeGroup(type, treeCopy) }
        }

        if (unresolvedSimpleTypes.isNotEmpty())
            throw Exception("Unresolved simple types (${unresolvedSimpleTypes.size}): $unresolvedSimpleTypes")

        if (unresolvedAttributes.isNotEmpty())
            throw Exception("Unresolved attributes (${unresolvedAttributes.size}): $unresolvedAttributes")

        if (unresolvedAttributeGroups.isNotEmpty())
            throw Exception("Unresolved attribute groups (${unresolvedAttributeGroups.size}): $unresolvedAttributeGroups")

        if (unresolvedComplexTypes.isNotEmpty()) {
            throw Exception("Unresolved complex types (${unresolvedComplexTypes.size}): $unresolvedComplexTypes")
        }

        if (unresolvedElements.isNotEmpty())
            throw Exception("Unresolved elements (${unresolvedElements.size}): $unresolvedElements")
    }

    private fun resolveSimpleType(type: XmlSchemaType, parent: XmlSchemaNode): Boolean {
        if (type !is XmlSchemaSimpleType) {
            return true
        }

        val node = createNode(type, parent)

        val resolved = resolveRestriction(type, node) &&
                resolveUnionFromSimpleType(type, node) &&
                resolveListFromSimpleType(type, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveComplexType(type: XmlSchemaType, parent: XmlSchemaNode): Boolean {
        if (type !is XmlSchemaComplexType) {
            return true
        }

        val node = createNode(type, parent)

        val resolvedType = type.contentModel?.let {
            resolveSimpleContent(type, node) && resolveComplexContent(type, node)
        } ?: true

        val resolvedParticle = type.particle?.let { resolveParticle(it, node) } ?: true

        val resolvedAttributes = resolveAttributedType(
            extractAttributeGroups(type),
            extractAttributeGroupRefs(type),
            extractAttributes(type),
            node
        )

        val resolved = resolvedType && resolvedParticle && resolvedAttributes
        return attachToParent(resolved, node, parent)
    }

    private fun resolveElement(type: XmlSchemaElement, parent: XmlSchemaNode): Boolean {
        val node = createNode(type, parent)
        val resolvedFromRef = resolveFromRef(type, node)
        val resolvedType = type.schemaType?.let {
            when (it.qName) {
                null -> resolveSimpleType(type.schemaType, node) &&
                        resolveComplexType(type.schemaType, node)

                else -> node.searchQName(it.qName) != null
            }
        } ?: true

        val resolved = resolvedType && resolvedFromRef
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttribute(type: XmlSchemaAttribute, parent: XmlSchemaNode): Boolean {
        val node = createNode(type, parent)
        val resolved = when (type.schemaTypeName) {
            null -> when (type.isRef) {
                true -> resolveFromRef(type, node)
                false -> resolveSimpleType(type.schemaType, node)
            }
            else -> parent.searchQName(type.schemaTypeName) != null
        }
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttributeGroup(type: XmlSchemaAttributeGroup, parent: XmlSchemaNode): Boolean {
        val node = createNode(type, parent)
        val resolved = resolveAttributedType(
            extractAttributeGroupsFromMembers(type.attributes),
            extractAttributeRefFromMembers(type.attributes),
            extractAttributesFromMembers(type.attributes),
            node
        )
        return attachToParent(resolved, node, parent)
    }

    private fun resolveParticle(particle: XmlSchemaParticle, parent: XmlSchemaNode) = when (particle) {
        is XmlSchemaChoice -> resolveChoiceParticle(particle, parent)
        is XmlSchemaSequence -> resolveSequenceParticle(particle, parent)
        is XmlSchemaAll -> resolveAllParticle(particle, parent)
        is XmlSchemaGroupRef -> resolveGroupRef(particle, parent)
        else -> throw Exception("Unsupported particle: ${particle::class.java}")
    }

    private fun resolveSequenceParticle(particle: XmlSchemaSequence, parent: XmlSchemaNode): Boolean {
        val node = createNode(particle, parent)
        val resolved = particle.items.fold(true) { acc, item -> acc && resolveObjectBase(item, node) }
        return attachToParent(resolved, node, parent)
    }

    private fun resolveChoiceParticle(particle: XmlSchemaChoice, parent: XmlSchemaNode): Boolean {
        val node = createNode(particle, parent)
        val resolved = particle.items.fold(true) { acc, item -> acc && resolveObjectBase(item, node) }
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAllParticle(particle: XmlSchemaAll, parent: XmlSchemaNode): Boolean {
        val node = createNode(particle, parent)
        val resolved = particle.items.fold(true) { acc, item -> acc && resolveObjectBase(item, node) }
        return attachToParent(resolved, node, parent)
    }

    private fun resolveObjectBase(item: XmlSchemaObjectBase, parent: XmlSchemaNode): Boolean = when (item) {
        is XmlSchemaSequence -> resolveSequenceParticle(item, parent)
        is XmlSchemaChoice -> resolveChoiceParticle(item, parent)
        is XmlSchemaElement -> resolveElement(item, parent)
        is XmlSchemaAll -> resolveAllParticle(item, parent)
        is XmlSchemaAny -> attachToParent(true, createNode(item, parent), parent)
        is XmlSchemaGroup -> resolveGroup(item, parent)
        is XmlSchemaGroupRef -> resolveGroupRef(item, parent)
        else -> throw Exception("Unsupported object base: ${item::class.java}")
    }

    private fun resolveFromRef(type: XmlSchemaElement, tree: XmlSchemaNode) = when (type.isRef) {
        true -> tree.searchQName(type.ref.targetQName) != null
        false -> true
    }

    private fun resolveFromRef(type: XmlSchemaAttribute, tree: XmlSchemaNode) = when (type.isRef) {
        true -> tree.searchQName(type.ref.targetQName) != null
        false -> true
    }

    private fun resolveSimpleTypes(types: List<XmlSchemaSimpleType>, tree: XmlSchemaNode) =
        types.stream().allMatch { resolveSimpleType(it, tree) }

    private fun resolveSimpleContent(type: XmlSchemaComplexType, parent: XmlSchemaNode): Boolean {
        if (type.contentModel !is XmlSchemaSimpleContent) {
            return true
        }

        val node = createNode(type.contentModel, parent)
        val resolved = resolveRestriction(type, node) && resolveExtension(type, node)
        return attachToParent(resolved, node, parent)
    }


    private fun resolveComplexContent(type: XmlSchemaComplexType, parent: XmlSchemaNode): Boolean {
        if (type.contentModel !is XmlSchemaComplexContent) {
            return true
        }

        val node = createNode(type.contentModel, parent)
        val resolved = resolveRestriction(type, node) && resolveExtension(type, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveGroupRef(type: XmlSchemaGroupRef, parent: XmlSchemaNode): Boolean {
        return resolveGroupParticle(type.particle, parent)
    }

    private fun resolveGroup(type: XmlSchemaGroup, parent: XmlSchemaNode): Boolean {
        val node = createNode(type, parent)
        val resolved = resolveGroupParticle(type.particle, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveGroupParticle(type: XmlSchemaGroupParticle, parent: XmlSchemaNode): Boolean {
        return when (type) {
            is XmlSchemaSequence -> resolveSequenceParticle(type, parent)
            is XmlSchemaChoice -> resolveChoiceParticle(type, parent)
            is XmlSchemaAll -> resolveAllParticle(type, parent)
            else -> throw Exception("Unknown particle in XmlSchemaGroup found: ${type::class.java}")
        }
    }

    private fun resolveExtension(type: XmlSchemaComplexType, parent: XmlSchemaNode): Boolean {
        val content = type.contentModel.content
        if (content !is XmlSchemaComplexContentExtension && content !is XmlSchemaSimpleContentExtension) {
            return true
        }

        val node = createNode(type.contentModel.content, parent)

        val baseQName = getBaseType(content)
        val parentTypeNode = parent.searchQName(baseQName)
        parentTypeNode?.let {
            parent.typeParentNode = it
            it.typeChildNodes.add(parent)
        } ?: return false

        val particleResolved = when (content is XmlSchemaComplexContentExtension) {
            true -> content.particle?.let { resolveParticle(it, node) } ?: true
            false -> true
        }

        val attributesResolved = resolveAttributedType(
            extractAttributeGroups(type.contentModel.content),
            extractAttributeGroupRefs(type.contentModel.content),
            extractAttributes(type.contentModel.content),
            node
        )

        val resolved = particleResolved && attributesResolved
        return attachToParent(resolved, node, parent)
    }

    private fun resolveRestriction(type: XmlSchemaSimpleType, parent: XmlSchemaNode): Boolean {
        val content = type.content
        if (content !is XmlSchemaSimpleTypeRestriction) {
            return true
        }

        val node = createNode(type.content, parent)
        val typeResolved = parent.searchQName(content.baseTypeName) != null
        val attributesResolved = resolveAttributedType(
            extractAttributeGroups(type.content),
            extractAttributeGroupRefs(type.content),
            extractAttributes(type.content),
            parent
        )

        val resolved = typeResolved && attributesResolved
        return attachToParent(resolved, node, parent)
    }

    private fun resolveRestriction(type: XmlSchemaComplexType, parent: XmlSchemaNode): Boolean {
        val content = type.contentModel.content
        if (content !is XmlSchemaComplexContentRestriction && content !is XmlSchemaSimpleContentRestriction) {
            return true
        }

        val node = createNode(type.contentModel.content, parent)
        val typeResolved = parent.searchQName(getBaseType(content)) != null
        val attributesResolved = resolveAttributedType(
            extractAttributeGroups(type.contentModel.content),
            extractAttributeGroupRefs(type.contentModel.content),
            extractAttributes(type.contentModel.content),
            node
        )
        val resolved = typeResolved && attributesResolved
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttributedType(
        attributeGroups: List<XmlSchemaAttributeGroup>,
        attributeGroupRefs: List<XmlSchemaAttributeGroupRef>,
        attributes: List<XmlSchemaAttribute>,
        parent: XmlSchemaNode
    ): Boolean {
        val resolvedAttributeGroups =
            attributeGroups.fold(true) { acc, attrGroup -> acc && resolveAttributeGroup(attrGroup, parent) }

        val resolvedAttributeGroupRefs = attributeGroupRefs.fold(true) { acc, attrGroupRef ->
            val node = createNode(attrGroupRef, parent)
            attachToParent(acc && node.searchQName(attrGroupRef.ref.targetQName) != null, node, parent)
        }

        val resolvedAttributes = attributes.fold(true) { acc, attr -> acc && resolveAttribute(attr, parent) }

        return resolvedAttributeGroups && resolvedAttributeGroupRefs && resolvedAttributes
    }

    private fun resolveUnionFromSimpleType(type: XmlSchemaSimpleType, parent: XmlSchemaNode): Boolean {
        val content = type.content
        if (content !is XmlSchemaSimpleTypeUnion) {
            return true
        }

        val node = createNode(type.content, parent)
        val resolvedMemberTypes =
            content.memberTypesQNames.fold(true) { acc, qName -> acc && parent.searchQName(qName) != null }
        val resolvedBaseTypes = resolveSimpleTypes(content.baseTypes, parent)

        val resolved = resolvedMemberTypes && resolvedBaseTypes
        return attachToParent(resolved, node, parent)
    }

    private fun resolveListFromSimpleType(type: XmlSchemaSimpleType, parent: XmlSchemaNode): Boolean {
        val content = type.content
        if (content !is XmlSchemaSimpleTypeList) {
            return true
        }

        val node = createNode(type.content, parent)
        val resolved = parent.searchQName(content.itemTypeName) != null
        return attachToParent(resolved, node, parent)
    }


    private fun createNode(type: XmlSchemaObjectBase, parent: XmlSchemaNode) = when (type) {
        is XmlSchemaNamed -> XmlSchemaNode(type.qName, type, parent)
        else -> XmlSchemaNode(null, type, parent)
    }.also { it.parent = parent }

    private fun attachToParent(resolved: Boolean, node: XmlSchemaNode, parent: XmlSchemaNode): Boolean {
        if (resolved) {
            parent.addChild(node)
        }
        return resolved
    }

    private inline fun <reified T> singleResolveLoop(
        unresolvedSource: MutableMap<QName, T>,
        doResolve: (T, XmlSchemaNode) -> Boolean
    ) {
        val unresolvedSourceCopy: MutableMap<QName, T> = HashMap(unresolvedSource)
        for ((qName, type) in unresolvedSourceCopy) {
            val nodeCopy = dependencyTree.clone()
            if (doResolve(type, nodeCopy)) {
                dependencyTree = nodeCopy
                unresolvedSource.remove(qName)
                logger.info("Resolved: $qName")
            }
        }
    }

    private inline fun <reified T : XmlSchemaNamed> createUnresolved(): MutableMap<QName, T> {
        // fill in deny list to omit implicit XML Schema data types, which seem to be part of each collection
        // others may be added if required
        val denyList: List<String> = listOf(
            "http://www.w3.org/2001/XMLSchema"
        )
        return schemaCollection.xmlSchemas
            .filter { !denyList.contains(it.targetNamespace) }
            .fold(mutableMapOf()) { accumulator, schema ->
                accumulator.apply {
                    putAll(schema.items
                        .asSequence()
                        .filterNotNull()
                        .filter { element: XmlSchemaObject -> element is T }
                        .map { element: XmlSchemaObject -> element as T }
                        .map { it.qName to it }
                        .toMap())
                }
            }
    }

    private fun extractAttributeGroupsFromMembers(attributeGroupMembers: List<XmlSchemaAttributeGroupMember>) =
        attributeGroupMembers.filterIsInstance(XmlSchemaAttributeGroup::class.java).toList()

    private fun extractAttributesFromMembers(attributeGroupMembers: List<XmlSchemaAttributeGroupMember>) =
        attributeGroupMembers.filterIsInstance(XmlSchemaAttribute::class.java).toList()

    private fun extractAttributeRefFromMembers(attributeGroupMembers: List<XmlSchemaAttributeGroupMember>) =
        attributeGroupMembers.filterIsInstance(XmlSchemaAttributeGroupRef::class.java).toList()

    private fun extractAttributeGroups(element: XmlSchemaObject): List<XmlSchemaAttributeGroup> =
        extractAttributesOrGroup(element)

    private fun extractAttributeGroupRefs(element: XmlSchemaObject): List<XmlSchemaAttributeGroupRef> =
        extractAttributesOrGroup(element)

    private fun extractAttributes(element: XmlSchemaObject): List<XmlSchemaAttribute> =
        extractAttributesOrGroup(element)

    private inline fun <reified T> extractAttributesOrGroup(element: XmlSchemaObject): List<T> = when (element) {
        is XmlSchemaComplexType ->
            element.attributes.filterIsInstance(T::class.java).toList()

        is XmlSchemaComplexContentRestriction ->
            element.attributes.filterIsInstance(T::class.java).toList()

        is XmlSchemaSimpleContentRestriction ->
            element.attributes.filterIsInstance(T::class.java).toList()

        is XmlSchemaComplexContentExtension ->
            element.attributes.filterIsInstance(T::class.java).toList()

        is XmlSchemaSimpleContentExtension ->
            element.attributes.filterIsInstance(T::class.java).toList()

        else -> emptyList()
    }

    private companion object : Logging {
        fun getBaseType(content: XmlSchemaContent): QName = when (content) {
            is XmlSchemaSimpleContentExtension -> content.baseTypeName
            is XmlSchemaComplexContentExtension -> content.baseTypeName
            is XmlSchemaSimpleContentRestriction -> content.baseTypeName
            is XmlSchemaComplexContentRestriction -> content.baseTypeName
            else -> throw Exception("XmlSchemaContent implementation unknown: ${content::class.java}")
        }
    }
}