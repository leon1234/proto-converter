package org.somda.protosdc.mapping.base

import org.apache.logging.log4j.kotlin.Logging
import org.w3c.dom.Attr
import org.w3c.dom.Element
import org.w3c.dom.Node
import java.math.BigDecimal
import java.math.BigInteger
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.time.YearMonth
import javax.xml.namespace.QName
import kotlin.time.Duration

typealias XmlToKotlinAnyHandler = (value: Node) -> kotlin.Any


@Suppress("MemberVisibilityCanBePrivate", "unused")
object XmlToKotlinBaseTypes : Logging {
    private var anyHandler: XmlToKotlinAnyHandler? = null

    fun mapAnyURI(value: Node): String {
        return value.textContent
    }

    fun mapBoolean(value: Node): Boolean {
        return when (val v = value.textContent) {
            "true", "1" -> true
            "false", "0" -> false
            else -> throw Exception("Malformed boolean; expected 'true', 'false', '1', '0' but found '$v'")
        }
    }

    fun mapDate(value: Node): LocalDate {
        return LocalDate.parse(value.textContent)
    }

    fun mapDateTime(value: Node): LocalDateTime {
        return LocalDateTime.parse(value.textContent)
    }

    fun mapDecimal(value: Node): BigDecimal {
        return value.textContent.toBigDecimal()
    }

    fun mapDuration(value: Node): Duration {
        return value.textContent?.let { Duration.parseIsoString(it) }
            ?: throw Exception("Malformed duration: ${value.textContent}")
    }

    fun mapGYearMonth(value: Node): YearMonth {
        return YearMonth.parse(value.textContent)
    }

    fun mapGYear(value: Node): Year {
        return Year.parse(value.textContent)
    }

    fun mapInt(value: Node): Int {
        return value.textContent.toInt()
    }

    fun mapInteger(value: Node): Long {
        return value.textContent.toLong()
    }

    fun mapLanguage(value: Node): String {
        return value.textContent
    }

    fun mapLong(value: Node): Long {
        return value.textContent.toLong()
    }

    fun mapQName(value: Node): QName {
        val parts = value.textContent.split(":")
        require(parts.size <= 2) { "Too many colons in QName: ${parts.size}" }
        return if (parts.size == 1) {
            value.lookupNamespaceURI(null)?.let {
                QName(it, parts.first())
            } ?: QName(parts.first())
        } else {
            val namespace = value.lookupNamespaceURI(parts.first())
            require(namespace != null) {
                "No namespace found for prefix ${parts.first()}"
            }
            QName(namespace, parts[1])
        }
    }

    fun mapString(value: Node): String {
        return value.textContent
    }

    fun mapUnsignedInt(value: Node): Int {
        return value.textContent.toInt()
    }

    fun mapUnsignedLong(value: Node): Long {
        return value.textContent.toLong()
    }

    fun mapAnySimpleType(value: Node): String {
        return value.textContent
    }

    fun mapAny(value: Node): kotlin.Any {
        val localAnyHandler = anyHandler
        return localAnyHandler?.let { localAnyHandler(value) } ?: value
    }

    fun registerAnyHandler(anyHandler: XmlToKotlinAnyHandler) {
        this.anyHandler = anyHandler
    }
}