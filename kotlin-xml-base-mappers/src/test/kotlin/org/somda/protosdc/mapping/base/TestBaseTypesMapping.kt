package org.somda.protosdc.mapping.base

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.w3c.dom.*
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.microseconds
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.nanoseconds
import kotlin.time.Duration.Companion.seconds

class TestBaseTypesMapping {
    @Test
    fun `test duration round trip`() {
        val durations = listOf(
            30.seconds,
            500.milliseconds,
            999999999.nanoseconds,
            0.minutes,
            5.minutes + 500.nanoseconds,
            3.seconds + 300.microseconds,
            7.hours + 56.minutes + 78.seconds + 100.milliseconds + 12000.nanoseconds,
            2.days,
            893473849383.microseconds
        )

        durations.forEach {
            val node = object : Node {
                private var value: String? = null

                override fun getNodeName(): String {
                    TODO("Not yet implemented")
                }

                override fun getNodeValue(): String? {
                    return value
                }

                override fun setNodeValue(nodeValue: String?) {
                    value = nodeValue
                }

                override fun getNodeType(): Short {
                    TODO("Not yet implemented")
                }

                override fun getParentNode(): Node {
                    TODO("Not yet implemented")
                }

                override fun getChildNodes(): NodeList {
                    TODO("Not yet implemented")
                }

                override fun getFirstChild(): Node {
                    TODO("Not yet implemented")
                }

                override fun getLastChild(): Node {
                    TODO("Not yet implemented")
                }

                override fun getPreviousSibling(): Node {
                    TODO("Not yet implemented")
                }

                override fun getNextSibling(): Node {
                    TODO("Not yet implemented")
                }

                override fun getAttributes(): NamedNodeMap {
                    TODO("Not yet implemented")
                }

                override fun getOwnerDocument(): Document {
                    TODO("Not yet implemented")
                }

                override fun insertBefore(newChild: Node?, refChild: Node?): Node {
                    TODO("Not yet implemented")
                }

                override fun replaceChild(newChild: Node?, oldChild: Node?): Node {
                    TODO("Not yet implemented")
                }

                override fun removeChild(oldChild: Node?): Node {
                    TODO("Not yet implemented")
                }

                override fun appendChild(newChild: Node?): Node {
                    TODO("Not yet implemented")
                }

                override fun hasChildNodes(): Boolean {
                    TODO("Not yet implemented")
                }

                override fun cloneNode(deep: Boolean): Node {
                    TODO("Not yet implemented")
                }

                override fun normalize() {
                    TODO("Not yet implemented")
                }

                override fun isSupported(feature: String?, version: String?): Boolean {
                    TODO("Not yet implemented")
                }

                override fun getNamespaceURI(): String {
                    TODO("Not yet implemented")
                }

                override fun getPrefix(): String {
                    TODO("Not yet implemented")
                }

                override fun setPrefix(prefix: String?) {
                    TODO("Not yet implemented")
                }

                override fun getLocalName(): String {
                    TODO("Not yet implemented")
                }

                override fun hasAttributes(): Boolean {
                    TODO("Not yet implemented")
                }

                override fun getBaseURI(): String {
                    TODO("Not yet implemented")
                }

                override fun compareDocumentPosition(other: Node?): Short {
                    TODO("Not yet implemented")
                }

                override fun getTextContent(): String? {
                    return value
                }

                override fun setTextContent(textContent: String?) {
                    value = textContent
                }

                override fun isSameNode(other: Node?): Boolean {
                    TODO("Not yet implemented")
                }

                override fun lookupPrefix(namespaceURI: String?): String {
                    TODO("Not yet implemented")
                }

                override fun isDefaultNamespace(namespaceURI: String?): Boolean {
                    TODO("Not yet implemented")
                }

                override fun lookupNamespaceURI(prefix: String?): String {
                    TODO("Not yet implemented")
                }

                override fun isEqualNode(arg: Node?): Boolean {
                    TODO("Not yet implemented")
                }

                override fun getFeature(feature: String?, version: String?): Any {
                    TODO("Not yet implemented")
                }

                override fun setUserData(key: String?, data: Any?, handler: UserDataHandler?): Any {
                    TODO("Not yet implemented")
                }

                override fun getUserData(key: String?): Any {
                    TODO("Not yet implemented")
                }

            }


            val intermediate = KotlinToXmlBaseTypes.mapDuration(it, node)
            val actual = XmlToKotlinBaseTypes.mapDuration(intermediate)
            assertEquals(it, actual)
        }
    }
}